package Exercises1;

import java.io.*;

public class Ejercicio1_2 {
    public static void main(String[] args) {
        try {
            FileReader fr = new FileReader("textFile.txt");
            int i = 0;
            while ((i = fr.read()) != -1) {
                System.out.print((char)i);
            }
            fr.close();
        } catch (Exception e) {System.out.println(e.getMessage());}
    }
}
