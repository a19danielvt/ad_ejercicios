package Exercises1;

import java.io.*;

public class Ejercicio1_1a{
	
	static String leerArchivo(String archivo){
		String texto = "";
		
		try{
			FileInputStream fi = new FileInputStream(archivo);    
			int i = 0;    
			while((i=fi.read())!=-1){    
			  texto += (char)i;    
			}
			fi.close(); 
		}catch(Exception e){System.out.println(e.getMessage());}

		return texto;
	}

	static void copiarTexto(String texto, String archivo){
		try {
			FileOutputStream fo = new FileOutputStream(archivo);
			byte[] b = texto.getBytes();
			fo.write(b);
			fo.close();
		} catch (Exception e) {System.out.println(e.getMessage());}
	}
	
	public static void main(String[] args){
		//Lee el String del archivo file1.txt y lo guarda en una variable
		String texto = leerArchivo(args[0]);
		//Escribir el String en modo bits en el nuevo archivo file2.txt
		copiarTexto(texto, args[1]);

		System.out.println("Este es el texto que contiene ahora " + args[1] + ":\n\t'" + leerArchivo(args[1]) + "'");
  	}
}
