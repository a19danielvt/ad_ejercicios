package Exercises1;

import java.io.*;
import java.util.Scanner;

public class Ejercicio1_3 {
    static Scanner sc = new Scanner(System.in);
    public static void main(String[] args) {
        String linea = "";
        try {
            FileWriter fw = new FileWriter(args[0]);
            while(true){
                System.out.println("Introduce una frase:");
                linea = sc.nextLine();

                //Cuando se introduzca "end", se cierra el bucle
                if(linea.compareTo("end") == 0){
                    fw.close();
                    break;
                } 

                //Si no escribe "end", sigue y guarda la linea en el archivo
                fw.write(linea + "\n");
            }
        } catch (Exception e) {System.out.println(e.getMessage());}
    }
}
