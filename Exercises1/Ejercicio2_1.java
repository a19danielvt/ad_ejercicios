package Exercises1;

import java.io.*;
import Exercises1.Ejercicio6_1.Product;

public class Ejercicio2_1 {
    public static void main(String[] args) {
        try{
            ObjectOutputStream oout = new ObjectOutputStream(new FileOutputStream("objetos.txt"));
            oout.writeObject(new Product(1, "descripción", 12));

            FileInputStream filein = new FileInputStream("objetos.txt");
            ObjectInputStream oin = new ObjectInputStream(filein);
            
            while(filein.available() > 0){
                Product p = (Product) oin.readObject();
                System.out.println(p.toString());
            }

            oout.close();
            oin.close();
        }catch(Exception ex){System.out.println(ex.getMessage());}
            
    }
}
