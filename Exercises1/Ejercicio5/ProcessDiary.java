package Exercises1.Ejercicio5;

import org.w3c.dom.*;

public class ProcessDiary {
    Document document;

    public ProcessDiary(){
        cargarArchivo("diary.xml");
        mostrarRoot();
        mostrarContenido();
        eliminarElemento("telephone");
        crearXml("diary1.xml");
        eliminarElemento("address");
        crearXml("diary2.xml");
        añadirElemento("email");
        crearXml("diary3.xml");
    }

    public void cargarArchivo(String nombre){
        try{
            document = XMLUtilities.XMLtoDOM(nombre);
        }catch(Exception e){}
    }

    public void mostrarRoot(){
        XMLUtilities.showRootElement(document);
    }

    public void mostrarContenido(){
        XMLUtilities.showElementContents(document.getDocumentElement());
    }

    public void eliminarElemento(String tagName){
        try{
            XMLUtilities.deleteElementsbyTag(tagName, document);
        }catch(Exception e){}
    }

    public void crearXml(String nombre){
        try{
            XMLUtilities.DOMtoXML(document, nombre);
        }catch(Exception e){}
    }

    public void añadirElemento(String tagName){
        String[] emails = {"email1", "email2", "email3", "email4", "email5", "email6"};

        NodeList contactos = document.getElementsByTagName("contact");

        for(int i = 0; i < contactos.getLength(); i++){
            contactos.item(i).appendChild(XMLUtilities.createTextElement(tagName, emails[i], document));
        }
    }
}
