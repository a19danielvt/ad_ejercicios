package Exercises1.Ejercicio5;

import java.io.*;

import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import org.w3c.dom.*;

public class XMLUtilities{
    static Document createEmptyDocument(String root) throws Exception{
        DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        Document document = db.getDOMImplementation().createDocument(null, root, null);
        document.setXmlVersion("1.0");

        return document;
    }

    static Element createTextElement(String tagName, String text, Document document){
        Element element = document.createElement(tagName);
        Text t = document.createTextNode(text);
        element.appendChild(t);

        return element;
    }

    static Document XMLtoDOM(String name) throws Exception {
        DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        Document document = db.parse(name);
        document.getDocumentElement().normalize();
        return document;
    }

    static void showRootElement(Document document){
        System.out.println("Elemento raiz: " + document.getDocumentElement().getNodeName());
    }

    static void showElementContents(Element element){
        // Si tiene atributos
        if(element.getAttributes() != null){
            NamedNodeMap mapaAtributos = element.getAttributes();
            for(int i = 0; i < mapaAtributos.getLength(); i++){
                System.out.println(mapaAtributos.item(i).getNodeName());
            }
        }
        // Contenido del elemento
        NodeList elemento = (NodeList) element;
        for (int i = 0; i < elemento.getLength();i++) {
            Node n = elemento.item(i);
            switch (n.getNodeType()){
                case Node.ELEMENT_NODE: 
                    Element e = (Element) n;
                    System.out.println("Etiqueta:" + e.getTagName());
                    showElementContents(e);
                    break;
                case Node.TEXT_NODE: 
                    Text t = (Text) n;
                    System.out.println("Texto:" + t.getWholeText());
                    break;
            }
        }
    }

    static void deleteElementsbyTag(String tagName, Document document) throws Exception{
        NodeList lista = document.getElementsByTagName(tagName);

        while(lista.getLength() > 0){
            Element e = (Element) lista.item(0);
            Element parent = (Element) e.getParentNode();
            parent.removeChild(e);
        }
    }

    static void DOMtoXML(Document document, String name) throws Exception{
        Source fuente = new DOMSource(document);
        Result resultado = new StreamResult(new File(name));

        Transformer t = TransformerFactory.newInstance().newTransformer();
        t.transform(fuente, resultado);
    }
}
