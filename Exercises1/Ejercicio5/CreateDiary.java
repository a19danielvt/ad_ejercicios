package Exercises1.Ejercicio5;

import org.w3c.dom.*;

public class CreateDiary {

    public CreateDiary(){
        crearDocumento();
    }

    public void crearDocumento(){
        String[] nombres = {"Dani", "Laira", "Alberto", "María", "Carlos", "Uxía"};
        String[] tlfs = {"111111", "222222", "333333", "444444", "555555", "666666"};
        String[] calles = {"Calle1", "Calle2", "Calle3", "Calle4", "Calle5", "Calle6"};
        String[] nCalles = {"1", "2", "3", "4", "5", "6"};
        try{
            Document document = XMLUtilities.createEmptyDocument("diary");

            for(int i = 0; i < nombres.length; i++){
                // raiz
                Element raiz = document.getDocumentElement();
                
                // añado contact a raiz
                Element contact = document.createElement("contact");
                contact.setAttribute("id", (i + 1) + "");
                raiz.appendChild(contact);

                // añado name a contact
                Element name = XMLUtilities.createTextElement("name", nombres[i], document);
                contact.appendChild(name);

                // añado telephone a contact
                Element telephone = XMLUtilities.createTextElement("telephone", tlfs[i], document);
                contact.appendChild(telephone);

                // añado address a contact
                Element address = document.createElement("address");
                contact.appendChild(address);

                // añado street a address
                Element street = XMLUtilities.createTextElement("street", calles[i], document);
                address.appendChild(street);

                //añado number a address
                Element number = XMLUtilities.createTextElement("number", nCalles[i], document);
                address.appendChild(number);

                XMLUtilities.DOMtoXML(document, "diary.xml");
            }
        }catch(Exception e){}
    }
}
