package Exercises1.Ejercicio6_2;

public class BookManagement {
    public static void main(String[] args) {
        BookUtilities.generateBookDat();
        BookUtilities.showBooks();
        BookUtilities.generateXmlBookDom();        
        BookUtilities.generateXmlBookXStream();
    }
}
