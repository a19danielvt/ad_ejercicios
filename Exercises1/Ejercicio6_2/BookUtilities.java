package Exercises1.Ejercicio6_2;

import java.io.*;
import java.util.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import org.w3c.dom.*;
import com.thoughtworks.xstream.XStream;


public class BookUtilities {
    public static void generateBookDat(){
        String[] isbns = {"11-111-1111-1", "22-222-2222-2", "33-333-3333-3", "44-444-4444-4", "55-555-5555-5", "66-666-6666-6"};
        String[] titles = {"Título 1", "Título 2", "Título 3", "Título 4", "Título 5", "Título 6"};
        ArrayList<ArrayList<String>> authors = new ArrayList<>(Arrays.asList(
            new ArrayList<>(Arrays.asList("Carlos", "Juan")),
            new ArrayList<>(Arrays.asList("Dani", "Mónica")),
            new ArrayList<>(Arrays.asList("María", "Pedro")),
            new ArrayList<>(Arrays.asList("Pepe", "Uxía")),
            new ArrayList<>(Arrays.asList("Manuel", "Paula")),
            new ArrayList<>(Arrays.asList("Miguel", "Felipe"))
        ));
        int[] years = {2000, 2003, 1999, 1997, 2010, 2017};
        boolean[] borroweds = {true, false, false, true, false, true};
        Person[] persons = {
            new Person("Diego", "11111111-A"),
            new Person("Carla", "22222222-B"),
            new Person("Gabriel", "33333333-C"),
            new Person("Fernando", "44444444-D"),
            new Person("Jacobo", "55555555-E"),
            new Person("Víctor", "66666666-F"),
        };

        try{
            ObjectOutputStream oo = new ObjectOutputStream(new FileOutputStream("books.dat"));

            for(int i = 0; i < isbns.length; i++){
                Book b = new Book(isbns[i], titles[i], authors.get(i), years[i], borroweds[i], persons[i]);
                oo.writeObject(b);
            }

            oo.close();
        }catch(Exception e){System.out.println(e.getMessage());}
    }

    public static void showBooks(){
        try {
            FileInputStream fi = new FileInputStream("books.dat");
            ObjectInputStream oi = new ObjectInputStream(fi);

            while(fi.available() > 0){
                Book b = (Book) oi.readObject();
                System.out.println(b + "\n");
            }

            fi.close(); oi.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static void generateXmlBookDom(){
        try{
            DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document document = db.getDOMImplementation().createDocument(null, "books", null);
            document.setXmlVersion("1.0");

            FileInputStream fi = new FileInputStream("books.dat");
            ObjectInputStream oi = new ObjectInputStream(fi);

            // Raíz del documento
            Element raiz = document.getDocumentElement();
            while(fi.available() > 0){
                Book b = (Book) oi.readObject();

                // Inserción de elementos
                // 'book'
                Element book = document.createElement("book");
                raiz.appendChild(book);

                // 'isbn'
                Element isbn = document.createElement("isbn");
                Text isbnText = document.createTextNode(b.getIsbn());
                isbn.appendChild(isbnText);
                book.appendChild(isbn);

                // 'title'
                Element title = document.createElement("title");
                Text titleText = document.createTextNode(b.getTitle());
                title.appendChild(titleText);
                book.appendChild(title);

                // 'authors'
                Element author = document.createElement("author");
                for(String autor : b.getAuthors()){
                    Element nombre = document.createElement("string");
                    Text nombreText = document.createTextNode(autor);
                    nombre.appendChild(nombreText);
                    author.appendChild(nombre);
                }
                book.appendChild(author);

                // 'year'
                Element year = document.createElement("year");
                Text yearText = document.createTextNode(b.getYear() + "");
                year.appendChild(yearText);
                book.appendChild(year);

                // 'borrowed'
                Element borrowed = document.createElement("borrowed");
                Text borrowedText = document.createTextNode(b.isBorrowed()? "si" : "no");
                borrowed.appendChild(borrowedText);
                book.appendChild(borrowed);

                // 'reader'
                Element reader = document.createElement("reader");
                // (name)
                Element name = document.createElement("name");
                Text nameText = document.createTextNode(b.getReader().getName());
                name.appendChild(nameText);
                reader.appendChild(name);
                // (dni)
                Element dni = document.createElement("dni");
                Text dniText = document.createTextNode(b.getReader().getDni());
                dni.appendChild(dniText);
                reader.appendChild(dni);
                
                book.appendChild(reader);
            }

            // Crea el archivo XML
            Source source = new DOMSource(document);
            Result result = new StreamResult(new File("booksDOM.xml"));
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.transform(source, result);
        }catch(Exception e){System.out.println(e.getMessage());}
    }

    public static void generateXmlBookXStream(){
        ArrayList<Book> books = new ArrayList<>();

        try {
            FileInputStream fi = new FileInputStream("books.dat");
            ObjectInputStream oi = new ObjectInputStream(fi);

            while(fi.available() > 0){
                Book b = (Book) oi.readObject();
                books.add(b);
            }

            fi.close(); oi.close();

            XStream x = new XStream();
            x.setMode(XStream.NO_REFERENCES);
            x.alias("books", List.class);
            x.alias("book", Book.class);
            x.toXML(books, new FileOutputStream("booksXStream.xml"));
        } catch (Exception e) {
            //TODO: handle exception
        }
    }
}
