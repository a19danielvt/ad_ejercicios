package Exercises1.Ejercicio6_2;

import java.io.Serializable;
import java.util.ArrayList;

public class Book implements Serializable{
    private static final long serialVersionUID = 1L;
    private String isbn;
    private String title;
    private ArrayList<String> authors = new ArrayList<>();
    private int year;
    private boolean borrowed;
    private Person reader;

    public Book(String isbn, String title, ArrayList<String> authors, int year, boolean borrowed, Person reader) {
        this.isbn = isbn;
        this.title = title;
        this.authors = authors;
        this.year = year;
        this.borrowed = borrowed;
        this.reader = reader;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList<String> getAuthors() {
        return authors;
    }

    public void setAuthors(ArrayList<String> authors) {
        this.authors = authors;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public boolean isBorrowed() {
        return borrowed;
    }

    public void setBorrowed(boolean borrowed) {
        this.borrowed = borrowed;
    }

    public Person getReader() {
        return reader;
    }

    public void setReader(Person reader) {
        this.reader = reader;
    }

    public String toString(){
        String texto = 
            "ISBN: " + getIsbn() + "\n" +
            "Título: " + getTitle() + "\n" +
            "Autores: ";

        for(int i = 0; i < getAuthors().size(); i++){
            if(i == getAuthors().size() - 1)
                texto += getAuthors().get(i);
            else
                texto += getAuthors().get(i) + ", ";
        }

        texto += "\n" +
            "Año: " + getYear() + "\n" +
            "Prestado: " + (isBorrowed()? "si" : "no") + "\n" + 
            "Lector: " + getReader().getName() + " [DNI: " + getReader().getDni() + "]";

        return texto;
    }
}