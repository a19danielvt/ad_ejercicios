package Exercises1;

import java.io.*;
import java.util.ArrayList;
import Exercises1.Ejercicio6_1.Product;

public class Ejercicio2_2 {
    public static void main(String[] args) {
        ArrayList<Product> productos = Binary.createProducts();
        Binary.storeProducts(productos);
        Binary.readProducts();
    }   
}



class Binary{
    static ArrayList<Product> createProducts(){
        ArrayList<Product> productos = new ArrayList<>();

        int[] identifiers = {123, 456, 789, 235, 567};
        String[] descriptions = {"coffee","milk","rice","salt","cocoa"};
        double[] prices = {1.22, 1.05, 1, 1.25, 3.2};

        Product p;
        for (int i = 0; i < identifiers.length; i++) {
            p = new Product(identifiers[i], descriptions[i], prices[i]);
            productos.add(p);
        }

        return productos;
    }

    static void storeProducts(ArrayList<Product> productos){
        try{
            ObjectOutputStream oout = new ObjectOutputStream(new FileOutputStream("products.dat"));
            for(Product p : productos){
                oout.writeObject(p);
            }

            oout.close();
        }catch(Exception e){System.out.println(e.getMessage());}
    }

    static void readProducts(){
        try {
            FileInputStream filein = new FileInputStream("products.dat");    
            ObjectInputStream oin = new ObjectInputStream(filein);

            while(filein.available() > 0){
                Product p = (Product) oin.readObject();
                System.out.println(p);
            }
            oin.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
