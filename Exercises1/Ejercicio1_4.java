package Exercises1;

import java.io.*;

public class Ejercicio1_4 {
    public static void main(String[] args) {
        int lineas = 0;
        try{
            BufferedReader br = new BufferedReader(new FileReader(args[0]));
            while(br.readLine() != null){
                lineas++;
            }
            br.close();
        }catch(Exception e){System.out.println(e.getMessage());}

        System.out.println("El número de líneas es de: " + lineas);
    }
}