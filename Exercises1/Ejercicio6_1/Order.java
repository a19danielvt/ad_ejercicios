package Exercises1.Ejercicio6_1;

import java.io.*;
import java.util.ArrayList;

public class Order implements Serializable{
    private static final long serialVersionUID = 1L;
    private int idOrder;
    private String clientName;
    private ArrayList<Product> productos = new ArrayList<>();

    public Order(int id, String name, ArrayList<Product> productos){
        this.idOrder = id;
        this.clientName = name;
        this.productos = productos;
    }

    public int getIdOrder() {
        return idOrder;
    }

    public void setIdOrder(int idOrder) {
        this.idOrder = idOrder;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public ArrayList<Product> getProductos() {
        return productos;
    }

    public void setProductos(ArrayList<Product> productos) {
        this.productos = productos;
    }

    public String toString(){
        String p = "";
        for(Product pr : productos){
            p += "\tid: " + pr.getIdProduct() + ", descripción: " + pr.getDescription() + ", precio: " + pr.getPrice() + "\n";
        }
        return "id: " + idOrder + ", cliente: " + clientName + ", productos: \n" + p;
    }
}
