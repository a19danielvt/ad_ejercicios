package Exercises1.Ejercicio6_1;

import java.io.*;
import java.util.*;

import com.thoughtworks.xstream.XStream;

public class Ejercicio6_1 {
    public static void main(String[] args) {
        try{
            ArrayList<Product> products = getArray();

            XStream x = new XStream();
            x.setMode(XStream.NO_REFERENCES);
            x.alias("producto", Product.class);
            x.alias("lista_de_productos", List.class);
            x.toXML(products, new FileOutputStream("products1.xml"));
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }

    public static ArrayList<Product> getArray(){
        ArrayList<Product> productos = new ArrayList<>();

        try{
            FileInputStream fi = new FileInputStream("products.dat");
            ObjectInputStream oi = new ObjectInputStream(fi);

            while(fi.available() > 0){
                System.out.println("copia objeto");
                Product producto = (Product) oi.readObject();
                productos.add(producto);
            }
            
            fi.close(); oi.close();
        }catch(Exception e){
            System.out.println(e.getMessage());
        }

        return productos;
    }
}