package Exercises1;

import java.io.*;

public class Ejercicio1_1b {
    public static void main(String[] args) {
        String frase = "";
        try {
            //Lee el contenido del archivo
            FileReader fr = new FileReader(args[0]);
            int i = 0;
            while ((i = fr.read()) != -1) {
                frase += (char)i;
            }
            fr.close();

            //Escribe el contenido en un nuevo archivo
            FileWriter fw = new FileWriter(args[1]);
            fw.write(frase);
            fr.close();
            fw.close();
        } catch (Exception e) {System.out.println(e.getMessage());}

        System.out.println("contenido escrito");
    }
}
