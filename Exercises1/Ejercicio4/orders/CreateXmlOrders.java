package Exercises1.Ejercicio4.orders;

import java.io.*;
import java.util.ArrayList;

import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import org.w3c.dom.*;

public class CreateXmlOrders {
    public static ArrayList<Order> createOrdersList() {
        int[] ids = { 1, 2, 3 };
        String[] nombres = { "Dani", "Manuel", "María" };
        ArrayList<Product> p1 = new ArrayList<>();
        p1.add(new Product(1, "cosa", 1.2));
        p1.add(new Product(2, "otra cosa", 43.2));
        p1.add(new Product(3, "mesa", 34.6));
        ArrayList<Product> p2 = new ArrayList<>();
        p2.add(new Product(4, "ordenador", 3.6));
        p2.add(new Product(5, "descripción", 43.67));
        p2.add(new Product(6, "ratón", 43.6));
        ArrayList<Product> p3 = new ArrayList<>();
        p3.add(new Product(7, "botella", 4.3));
        p3.add(new Product(8, "teclado", 54.6));
        p3.add(new Product(9, "otradescripción", 43.65));

        ArrayList<ArrayList<Product>> lista = new ArrayList<>();
        lista.add(p1);
        lista.add(p2);
        lista.add(p3);

        ArrayList<Order> orders = new ArrayList<>();
        for (int i = 0; i < nombres.length; i++) {
            orders.add(new Order(ids[i], nombres[i], lista.get(i)));
        }

        return orders;
    }

    public static void storeOrders(ArrayList<Order> orders) {
        try {
            ObjectOutputStream oout = new ObjectOutputStream(new FileOutputStream("orders.txt"));
            for (Order o : orders) {
                oout.writeObject(o);
            }
            oout.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void readOrdersFile() {
        try {
            FileInputStream file = new FileInputStream("orders.txt");
            ObjectInputStream oin = new ObjectInputStream(file);

            while (file.available() > 0) {
                Order o = (Order) oin.readObject();
                System.out.println(o);
            }
            oin.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static ArrayList<Order> readOrders() {
        ArrayList<Order> orders = new ArrayList<>();

        try {
            FileInputStream file = new FileInputStream("orders.txt");
            ObjectInputStream oin = new ObjectInputStream(file);

            while (file.available() > 0) {
                Order o = (Order) oin.readObject();
                orders.add(o);
            }
            oin.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return orders;
    }

    public static void createOrdersXml(ArrayList<Order> orders) {
        try {
            FileInputStream file = new FileInputStream("orders.txt");
            ObjectInputStream oin = new ObjectInputStream(file);

            // Crea el root del xml
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document documento = builder.getDOMImplementation().createDocument(null, "orders", null);

            documento.setXmlVersion("1.0");

            while (file.available() > 0) {
                Order o = (Order) oin.readObject();

                // Primer elemento
                Element order = documento.createElement("order");
                documento.getDocumentElement().appendChild(order);

                // Elemento idOrder con su valor
                Element idOrder = documento.createElement("idOrder");
                order.appendChild(idOrder);
                Text idtText = documento.createTextNode(o.getIdOrder() + "");
                idOrder.appendChild(idtText);

                // Elemento client con su valor
                Element client = documento.createElement("clientName");
                order.appendChild(client);
                Text name = documento.createTextNode(o.getClientName());
                client.appendChild(name);

                // Cuarto elemento
                Element products = documento.createElement("products");
                order.appendChild(products);

                // Todos los productos del order correspondiente
                for (Product p : o.getProductos()) {
                    // Un elemento product para cada producto
                    Element product = documento.createElement("product");
                    products.appendChild(product);

                    // Elemento idProduct con su valor
                    Element idProduct = documento.createElement("idProduct");
                    product.appendChild(idProduct);
                    Text idpText = documento.createTextNode(p.getIdProduct() + "");
                    idProduct.appendChild(idpText);

                    // Elemento description y su valor
                    Element description = documento.createElement("description");
                    product.appendChild(description);
                    Text dText = documento.createTextNode(p.getDescription());
                    description.appendChild(dText);

                    // Elemento price y su valor
                    Element price = documento.createElement("price");
                    product.appendChild(price);
                    Text pText = documento.createTextNode(p.getPrice() + "");
                    price.appendChild(pText);
                }
            }
            oin.close();

            Source fuente = new DOMSource(documento);
            Result result = new StreamResult(new File("orders.xml"));

            Transformer t = TransformerFactory.newInstance().newTransformer();
            t.transform(fuente, result);

            /**
             * DataOutputStream dos = new DataOutputStream(new
             * FileOutputStream("orders.xml")); dos.writeUTF(DOM2XML(documento));
             */

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * public static String DOM2XML(Document doc) { String xmlString = null; try {
     * TransformerFactory tf = TransformerFactory.newInstance(); Transformer t =
     * tf.newTransformer(); t.setOutputProperty(OutputKeys.INDENT, "yes");
     * t.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
     * //t.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
     * 
     * StreamResult result = new StreamResult(new StringWriter()); DOMSource source
     * = new DOMSource(doc); t.transform(source, result); xmlString =
     * result.getWriter().toString(); } catch (TransformerException ex) {
     * System.out.println("Error al pasar DOM a XML"); xmlString = null; } return
     * xmlString; }
     * 
     */

    public static void readOrdersXml() throws Exception {
        DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        Document document = db.parse(new File("orders.xml"));
        document.getDocumentElement().normalize();

        NodeList order = document.getElementsByTagName("order");
        for(int i = 0; i < order.getLength(); i++){
            Element orderElement = (Element) order.item(i);
            Element id = (Element) orderElement.getElementsByTagName("idOrder").item(0);
            Element clientName = (Element) orderElement.getElementsByTagName("clientName").item(0);
            
            System.out.println("idOrder: " + id.getTextContent());
            System.out.println("clientName: " + clientName.getTextContent());
            
            Element products = (Element) orderElement.getElementsByTagName("products").item(0);
            NodeList product = products.getElementsByTagName("product");
            for(int j = 0; j < product.getLength(); j++){
                System.out.println("-> Producto " + (j + 1) + ":");
                Element productElement = (Element) product.item(j);
                Element idProduct = (Element) productElement.getElementsByTagName("idProduct").item(0);
                Element description = (Element) productElement.getElementsByTagName("description").item(0);
                Element price = (Element) productElement.getElementsByTagName("price").item(0);

                System.out.println("\tidProduct: " + idProduct.getTextContent());
                System.out.println("\tdescription: " + description.getTextContent());
                System.out.println("\tprice: " + price.getTextContent());
            }
            System.out.println();
        }
        
        /**
        try {
            DataInputStream dis = new DataInputStream(new FileInputStream("orders.xml"));
            System.out.println(dis.readUTF());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
         */
    }
}
