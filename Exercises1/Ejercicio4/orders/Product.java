package Exercises1.Ejercicio4.orders;

import java.io.Serializable;

class Product implements Serializable {
    private static final long serialVersionUID = 1L;
    private int idProduct;
    private String description;
    private double price;

    public Product(int id, String des, double price){
        this.idProduct = id;
        this.description = des;
        this.price = price;
    }

    public int getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(int idProduct) {
        this.idProduct = idProduct;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
    
    public String toString(){
        return "id: " + idProduct + ", Descripción: " + description + ", precio: " + price;
    }
}
