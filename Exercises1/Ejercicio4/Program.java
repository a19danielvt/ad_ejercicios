package Exercises1.Ejercicio4;

import java.util.ArrayList;

import Exercises1.Ejercicio4.orders.*;

public class Program {
    public static void main(String[] args) {
        try{
            ArrayList<Order> orders = CreateXmlOrders.createOrdersList();
            CreateXmlOrders.storeOrders(orders);
            CreateXmlOrders.readOrdersFile();
            CreateXmlOrders.createOrdersXml(CreateXmlOrders.readOrders());
            CreateXmlOrders.readOrdersXml();
        }catch(Exception e){System.out.println(e.getMessage());}
    }
}
