import java.io.*;

public class Prueba{
  public static void main(String[] args){
//Introducir String
    try{    
      FileOutputStream fout=new FileOutputStream("testout.txt");    
      String s="Welcome to javaTpoint.";    
      byte b[]=s.getBytes();//converting string into byte array    
      fout.write(b);    
      fout.close();   
      System.out.println("success..."); 
    }catch(Exception e){System.out.println(e);}    

//Leer String
    try{    
      FileInputStream fin=new FileInputStream("testout.txt");    
      int i = fin.read();  
      System.out.println((char)i);    
  
      fin.close();    
    }catch(Exception e){System.out.println(e);}

//Leer String completo
    try{    
      FileInputStream fin=new FileInputStream("testout.txt");    
      int i=0;    
      while((i=fin.read())!=-1){    
        System.out.print((char)i);    
      }    
      System.out.println("");
      fin.close();    
    }catch(Exception e){System.out.println(e);}

//Escribir con FileWriter
    try{    
      FileWriter fw=new FileWriter("testout.txt");    
      fw.write("Welcome to javaTpoint."); 
      fw.close();

//Leer con FileReader
    FileReader fr=new FileReader("testout.txt");    
    int i;    
    while((i=fr.read())!=-1)    
      System.out.print((char)i); 
    fr.close();
    }catch(Exception e){System.out.println(e);}    
    System.out.println("\nSuccess...");   


  }
}
