import java.util.*;
import clases.*;

public class Main {

	static Operations op = new Operations();
	static Scanner sc = new Scanner(System.in);
	
	public static void main(String[] args) {
		try {
			principal:
			while(true) {
				op.openConnection();
				
				System.out.println("1. Lista de pedidos");
				System.out.println("2. Añadir pedido");
				System.out.println("3. Modificar pedido");
				System.out.println("4. Eliminar pedido");
				System.out.println("Other. Salir");
				
				System.out.print("\nElige una opción: ");int option = sc.nextInt();
				System.out.println();
				
				switch(option) {
				case 1:
					showOrders();
					break;
				case 2:
					addOrder();
					break;
				case 3:
					modifyOrder();
					break;
				case 4:
					deleteOrder();
					break;
				default:
					System.out.println("BYE");
					break principal;
				}
				
			}
			
			sc.close();
			op.closeConnection();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	public static void showOrders() throws Exception{
		List<Pedido2> pedidos = op.ordersList();
		
		for(Pedido2 p : pedidos) {
			System.out.println("Id: " + p.getIdPedido());
			System.out.println("\tCliente: " + p.getCliente2().getNome());
			System.out.println("\tImporte: " + p.getImporte());
			
			Set<LineaPedido2> productos = p.getLineaPedido2s();
			System.out.println("\tProductos:");
			
			double precioTotal = 0;
			for(Object o : productos) {
				LineaPedido2 lp = (LineaPedido2) o;
				double precio = lp.getProduto2().getPrezo();
				double cantidad = lp.getCantidade();
				System.out.println("\t\tProducto: " + lp.getProduto2().getNome() + ", precio: " + precio + ", cantidad: " + cantidad);
				
				precioTotal += precio * cantidad;
			}

			System.out.println("\tTotal: " + precioTotal);
		}
		
		System.out.println();
	}

	public static void addOrder() throws Exception{
		// Cliente
		Cliente2 client = null;
		while(true) {
			System.out.print("Id del cliente: "); int idCliente = sc.nextInt();
		
			try {
				client = op.getClient(idCliente);
				break;
			}catch(Exception e) {
				System.out.println(e.getMessage());
			}
		}
		
		//Importe
		System.out.print("Importe total: "); double importe = sc.nextDouble();
		System.out.println();

		// Producto y Cantidad
		System.out.println("Productos: ");
		System.out.println("Introduce '0' en el 'Id del producto' para dejar de comprar.");
		System.out.println();
		
		Set<LineaPedido2> lineas = new HashSet<>();
		double pagoTotal = 0;
		while(true) {
			// Producto
			System.out.print("Id del producto: "); int id = sc.nextInt();
			
			if(id == 0) {
				System.out.println();
				break;
			}

			Produto2 product = null;
			try {
				product = op.getProduct(id);
			}catch(Exception e) {
				System.out.println(e.getMessage());
				continue;
			}
			
			// Cantidad
			System.out.print("Cantidad: "); double cantidad = sc.nextDouble();
			
			// Comprobación
			double temp = pagoTotal;
			temp += product.getPrezo() * cantidad;
			
			if(temp > importe) {
				System.out.println();
				System.out.println("***NO SE PUEDE REALIZAR LA COMPRA*** ");
				System.out.println("El pago actual es de " + temp + " y el importe es de " + importe +". Te quedan " + (importe - pagoTotal));
				System.out.println();
				continue;
			}

			// Todo bien
			pagoTotal = temp;
			
			lineas.add(new LineaPedido2(op.getProduct(id), cantidad));
			System.out.println();
		}

		Pedido2 p = new Pedido2(client, importe, lineas);
		op.addOrder(p);
		
		System.out.println("PEDIDO AÑADIDO");
		
		System.out.println();
	}
	
	public static void modifyOrder() throws Exception{
		Pedido2 order = null;
		
		while(true) {
			try {
				System.out.print("Introduce el Id del pedido: "); int id = sc.nextInt();
				order = op.getOrder(id);
				break;
			}catch(Exception e) {
				System.out.println(e.getMessage());
			}
		}
		
		System.out.println("Datos del pedido:");
		System.out.println("\tCliente: " + order.getCliente2().getNome());
		System.out.println("\tImporte: " + order.getImporte());
		System.out.println("\tProductos: ");
		
		double precioTotal = 0;
		for(LineaPedido2 lp : order.getLineaPedido2s()) {
			double precio = lp.getProduto2().getPrezo();
			double cantidad = lp.getCantidade();
			System.out.println("\t\tProducto: " + lp.getProduto2().getNome() + ", precio: " + precio + ", cantidad: " + cantidad);
			
			precioTotal += precio * cantidad;
		}
		System.out.println("\tTotal: " + precioTotal);
		System.out.println();
		
		System.out.println("Elige:");
		
		ArrayList<Integer> idsValidos = new ArrayList<>();
		
		Set<LineaPedido2> products = order.getLineaPedido2s();
		
		ArrayList<LineaPedido2> temp = new ArrayList<>();
		for(LineaPedido2 p : products) {
			temp.add(p);
			int id = p.getProduto2().getIdProduto();
			idsValidos.add(id);
			System.out.println("\t" + id + ". " + p.getProduto2().getNome());
		}
		
		int idProduct = -1;
		while(true) {
			System.out.print("-> ");
			idProduct = sc.nextInt();
			
			if(idsValidos.contains(idProduct)) break;
			else System.out.println("INVALID ID");
		}
		
		System.out.println("1. Eliminar producto");
		System.out.println("2. Cambiar cantidad");
		
		principal:
		while(true) {
			System.out.print("Elige una opción: "); int option = sc.nextInt();
			switch(option) {
			case 1:
				deleteProduct(order, temp, idProduct);
				break principal;
			case 2:
				modifyProduct(order, temp, idProduct);
				break principal;
			}
		}
		
		System.out.println("\nMODIFICADO\n");
	}
	
	public static void deleteProduct(Pedido2 order, ArrayList<LineaPedido2> temp, int idProduct) throws Exception{
		Set<LineaPedido2> newProducts = new HashSet<>();
		for(LineaPedido2 lp : temp) {
			if(idProduct == lp.getProduto2().getIdProduto())
				lp.setCantidade(0.0);
			
			newProducts.add(lp);
		}
		
		order.setLineaPedido2s(newProducts);
		op.modifyOrder(order);
	}
	
	public static void modifyProduct(Pedido2 order, ArrayList<LineaPedido2> temp, int idProduct) throws Exception{
		System.out.print("Nueva cantidad: "); double cantidad = sc.nextDouble();
		
		Set<LineaPedido2> newProducts = new HashSet<>();
		for(LineaPedido2 lp : temp) {
			if(idProduct == lp.getProduto2().getIdProduto()) {
				lp.setCantidade(cantidad);
			}
			newProducts.add(lp);
		}

		order.setLineaPedido2s(newProducts);
		op.modifyOrder(order);
	}

	public static void deleteOrder() throws Exception{
		Pedido2 order = null;
		
		while(true) {
			System.out.print("Id del pedido: "); int id = sc.nextInt();
			try {
				order = op.getOrder(id);
				break;
			}catch(Exception e) {
				System.out.println(e.getMessage());
			}
		}
		
		op.deleteOrder(order);
		
		System.out.println("\nPEDIDO ELIMINADO\n");
	}
}










































