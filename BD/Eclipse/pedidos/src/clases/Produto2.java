package clases;
// Generated 18 dic. 2020 16:22:49 by Hibernate Tools 5.2.12.Final

import java.util.HashSet;
import java.util.Set;

/**
 * Produto2 generated by hbm2java
 */
public class Produto2 implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer idProduto;
	private String nome;
	private String descricion;
	private Double prezo;
	private String foto;
	private Set<LineaPedido2> lineaPedido2s = new HashSet<>(0);

	public Produto2() {
	}

	public Produto2(String nome, String descricion, Double prezo, String foto, Set<LineaPedido2> lineaPedido2s) {
		this.nome = nome;
		this.descricion = descricion;
		this.prezo = prezo;
		this.foto = foto;
		this.lineaPedido2s = lineaPedido2s;
	}

	public Integer getIdProduto() {
		return this.idProduto;
	}

	public void setIdProduto(Integer idProduto) {
		this.idProduto = idProduto;
	}

	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricion() {
		return this.descricion;
	}

	public void setDescricion(String descricion) {
		this.descricion = descricion;
	}

	public Double getPrezo() {
		return this.prezo;
	}

	public void setPrezo(Double prezo) {
		this.prezo = prezo;
	}

	public String getFoto() {
		return this.foto;
	}

	public void setFoto(String foto) {
		this.foto = foto;
	}

	public Set<LineaPedido2> getLineaPedido2s() {
		return this.lineaPedido2s;
	}

	public void setLineaPedido2s(Set<LineaPedido2> lineaPedido2s) {
		this.lineaPedido2s = lineaPedido2s;
	}

	@Override
	public String toString() {
		return "Produto2 [idProduto=" + idProduto + ", nome=" + nome + ", descricion=" + descricion + ", prezo=" + prezo
				+ ", foto=" + foto + ", lineaPedido2s=" + lineaPedido2s + "]";
	}

}
