package clases;

import javax.persistence.TypedQuery;
import java.util.*;
import org.hibernate.*;

public class Operations {
	private Session session;
	private Transaction ts;
	
	public void openConnection() throws Exception{
		SessionFactory factory = HibernateUtil.getSessionFactory();
		session = factory.getCurrentSession();

		if(session.getTransaction().isActive()) ts = session.getTransaction();
		else ts = session.beginTransaction();
	}
	
	public void closeConnection() throws Exception{
		session.close();
	}
	
	public List<Pedido2> ordersList() throws Exception{
		String sentencia = "from Pedido2";
		
		TypedQuery<Pedido2> datos = session.createQuery(sentencia, Pedido2.class);
		
		return datos.getResultList();
	}
	
	public void addOrder(Pedido2 order) throws Exception{
		session.save(order);
		ts.commit();

		openConnection();
		List<Pedido2> list = ordersList();
		int idOrder = list.get(list.size() - 1).getIdPedido();
		addOrderLines(idOrder, order.getLineaPedido2s());
	}
	
	public void addOrderLines(int id, Set<LineaPedido2> lineas) throws Exception{
		openConnection();
		for(Object o : lineas) {
			LineaPedido2 lp = (LineaPedido2) o;
			lp.setPedido2(getOrder(id));
			session.save(lp);
			ts.commit();
			openConnection();
		}
	}
	
	public void modifyOrder(Pedido2 order) throws Exception{
		for(LineaPedido2 lp : order.getLineaPedido2s()) {
			if(lp.getCantidade() < 1) {
				session.delete(lp);
			}
			else {
				session.update(lp);
			}
			ts.commit();
			openConnection();
		}
	}
	
	public void deleteOrder(Pedido2 order) throws Exception{
		for(LineaPedido2 p : order.getLineaPedido2s()) {
			session.delete(p);
			ts.commit();
			openConnection();
		}
		
		session.delete(order);
		ts.commit();
		
	}
	
	public Pedido2 getOrder(int id) throws Exception{
		Pedido2 p = session.get(Pedido2.class, id);
		
		if(p == null) throw new Exception("INVALID ID");
		
		return p;
	}
	
	public Produto2 getProduct(int id) throws Exception{
		Produto2 p = session.get(Produto2.class, id);
		
		if(p == null) throw new Exception("INVALID ID");
		
		return p;
	}
	
	public Cliente2 getClient(int id) throws Exception{
		Cliente2 c = session.get(Cliente2.class, id);
		
		if(c == null) throw new Exception("INVALID ID");
		
		return c;
	}
}
