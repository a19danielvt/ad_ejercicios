import org.xmldb.api.base.*;
import org.xmldb.api.modules.*;
import org.xmldb.api.*;
import javax.xml.transform.OutputKeys;
import java.io.File;

public class Main {

	public static void main(String[] args) {
//		createCollection();
		query();
	}
	
	public static void query() {
		try {
			String driver = "org.exist.xmldb.DatabaseImpl"; //Driver para eXist
			Class c1 = Class.forName(driver); //Carga o driver
			
			Database database = (Database) c1.newInstance();
			database.setProperty("create-database", "true");
	        DatabaseManager.registerDatabase(database);
	        
	        String URI = "xmldb:exist://localhost:8080/exist/xmlrpc/db/gimnasio";
	        
	        // Para hacer consultas no se necesita usuario y contraseña
	        Collection col = DatabaseManager.getCollection(URI);
	        col.setProperty(OutputKeys.INDENT, "no");
	        
	        // Preparar servicio
	        XQueryService xq = (XQueryService) col.getService("XQueryService", "1.0");
	        xq.setProperty("indent", "yes");
	        
	        // Query
	        CompiledExpression expression = xq.compile(
	        		"for $em in doc(\"/db/gimnasio/empleados.xml\")/EMPLEADOS/EMP_ROW\n"
	        		+ "return $em"
	        );
	        
	        // Ejecución
	        ResourceSet result = xq.execute(expression);
	        
	        // Recorrer
	        ResourceIterator iterator = result.getIterator();
	        
	        while(iterator.hasMoreResources()) {
	        	Resource res = iterator.nextResource();
	        	System.out.println(res.getContent());
	        }
	        
	        col.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void createCollection() {
		try {
			String driver = "org.exist.xmldb.DatabaseImpl"; //Driver para eXist
			Class c1 = Class.forName(driver); //Carga o driver
			
			Database database = (Database) c1.newInstance();
			database.setProperty("create-database", "true");
	        DatabaseManager.registerDatabase(database);
	        
	        String URI = "xmldb:exist://localhost:8080/exist/xmlrpc/db";
	        String usu = "admin"; //usuario
	        String usuPwd = "abc123.."; //clave
	        
	        Collection col = DatabaseManager.getCollection(URI,usu,usuPwd);
	        col.setProperty(OutputKeys.INDENT, "no");
	        
	        if (col == null)
	        	System.out.println("A coleccion non existe.");
	        
	        //Crear unha nova coleccion
	        CollectionManagementService mgtService =(CollectionManagementService)col.getService("CollectionManagementService","1.0");
//	        mgtService.createCollection("novaColeccion2");
	        
	        //Borrar unha coleccion
//	        mgtService.removeCollection("novaColeccion2");
	        
	        //Acceder á colección que acabo de engadir
	        URI = "xmldb:exist://localhost:8080/exist/xmlrpc/db/novaColeccion2";
	        col = DatabaseManager.getCollection(URI,usu,usuPwd);
	        
	        //Engadir un documento á colección
	        File arquivo = new File("departamentos.xml");
	        
	        if(!arquivo.canRead()) {
	        	System.out.println("Erro ó ler o ficheiro");
	        }
	        else {
	        	Resource novoRecurso = col.createResource(arquivo.getName(), "XMLResource");
	        	novoRecurso.setContent(arquivo);
	        	col.storeResource(novoRecurso);
	        }
	        
	        //Borrar un documento dunha coleccion
	        Resource recursoParaBorrar = col.getResource("departamentos.xml");
	        //col.removeResource(recursoParaBorrar);
	        
	        col.close();
		}
		catch(Exception erro) {
			System.out.println("Erro en creaColeccion "+erro.getMessage());
		}
	}
}
