package baseDatos;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Iterator;

import javax.xml.transform.OutputKeys;

import org.xmldb.api.DatabaseManager;
import org.xmldb.api.base.Collection;
import org.xmldb.api.base.CompiledExpression;
import org.xmldb.api.base.Database;
import org.xmldb.api.base.Resource;
import org.xmldb.api.base.ResourceIterator;
import org.xmldb.api.base.ResourceSet;
import org.xmldb.api.modules.CollectionManagementService;
import org.xmldb.api.modules.XQueryService;

public class Operations {
	
	Collection col;
    String user = "admin";
    String pass = "abc123..";
	
	public void openConnection() {
		try {
			String driver = "org.exist.xmldb.DatabaseImpl"; //Driver para eXist
			Class c1 = Class.forName(driver); //Carga o driver
			
			Database database = (Database) c1.newInstance();
			database.setProperty("create-database", "true");
	        DatabaseManager.registerDatabase(database);
	        
	        String URI = "xmldb:exist://localhost:8080/exist/xmlrpc/db";
	        
	        col = DatabaseManager.getCollection(URI, user, pass);
	        col.setProperty(OutputKeys.INDENT, "no");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void closeConnection() {
		try {
			col.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void createCollection() {
		try {
			CollectionManagementService service = (CollectionManagementService) col.getService("CollectionManagementService", "1.0");
			service.createCollection("GIMNASIO");
			System.out.println("COLECCIÓN CREADA");
			
	        String URI = "xmldb:exist://localhost:8080/exist/xmlrpc/db/GIMNASIO";
	        col = DatabaseManager.getCollection(URI, user, pass);
			
			String[] archivos = {"archivos/actividades_gim.xml", "archivos/socios_gim.xml", "archivos/uso_gimnasio.xml"};
			
			for(int i = 0; i < archivos.length; i++) {
				File file = new File(archivos[i]);
				
				if(!file.canRead()) {
					System.out.println("NO SE PUEDE LEER EL ARCHIVO");
				}else {
					Resource recurso = col.createResource(file.getName(), "XMLResource");
					recurso.setContent(file);
					col.storeResource(recurso);
					System.out.println("ARCHIVO INTRODUCIDO");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void createUsoGimnasio() {
		String datos = printDocument();
		System.out.println(datos);
		
		try {
	        String URI = "xmldb:exist://localhost:8080/exist/xmlrpc/db/GIMNASIO";
	        col = DatabaseManager.getCollection(URI, user, pass);
	        
			String fileName = "uso_gimnasio_modificado.xml";
			PrintWriter file = new PrintWriter(new FileWriter(fileName));
			file.println(datos);
			file.close();
			
			Resource recurso = col.createResource(fileName, "XMLResource");
			recurso.setContent(new File(fileName));
			col.storeResource(recurso);
			
			System.out.println("NUEVO ARCHIVO INTRODUCIDO");
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public String printDocument() {
		String datos = "";
		
		try {
            XQueryService xq = (XQueryService) col.getService("XQueryService", "1.0");
			xq.setProperty("indent", "yes");
			
	        // Query
	        CompiledExpression expression = xq.compile(
					"<USO_GIMNASIO>\n"
					+ "    {\n"
					+ "        for $fila in doc(\"/db/GIMNASIO/uso_gimnasio.xml\")//fila_uso\n"
					+ "            return\n"
					+ "                <fila_uso>\n"
					+ "                    <SOCIO>{data(doc(\"/db/GIMNASIO/socios_gim.xml\")//fila_socios[COD = $fila/CODSOCIO]/NOMBRE)}</SOCIO>\n"
					+ "                    <ACTIV>{data(doc(\"/db/GIMNASIO/actividades_gim.xml\")//fila_actividades[@cod = $fila/CODACTIV]/NOMBRE)}</ACTIV>\n"
					+ "                    {$fila/FECHA}\n"
					+ "                    {$fila/HORAINICIO}\n"
					+ "                    {$fila/HORAFINAL}\n"
					+ "                </fila_uso>\n"
					+ "    }\n"
					+ "</USO_GIMNASIO>"
	        );
	        
	        // Ejecución
	        ResourceSet result = xq.execute(expression);
	        
	        // Recorrer
	        ResourceIterator iterator = result.getIterator();
	        
	        while(iterator.hasMoreResources()) {
	        	Resource res = iterator.nextResource();
	        	datos += res.getContent();
	        }
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return datos;
	}
}










































