import baseDatos.Operations;

public class Main {

	public static void main(String[] args) {
		Operations op = new Operations();
		
		op.openConnection();
		
		op.createCollection();
		op.createUsoGimnasio();
		
		op.closeConnection();
	}

}
