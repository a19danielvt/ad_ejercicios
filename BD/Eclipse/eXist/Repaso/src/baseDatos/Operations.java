package baseDatos;

import javax.xml.transform.OutputKeys;

import org.xmldb.api.DatabaseManager;
import org.xmldb.api.base.Collection;
import org.xmldb.api.base.CompiledExpression;
import org.xmldb.api.base.Database;
import org.xmldb.api.base.Resource;
import org.xmldb.api.base.ResourceIterator;
import org.xmldb.api.base.ResourceSet;
import org.xmldb.api.modules.XQueryService;

public class Operations {
	Collection col;
	
	String user = "admin";
	String pass = "abc123..";
	
	public void openConnection() {
		try {
			String driver = "org.exist.xmldb.DatabaseImpl"; //Driver para eXist
			Class c1 = Class.forName(driver); //Carga o driver
			
			Database database = (Database) c1.newInstance();
			database.setProperty("create-database", "true");
	        DatabaseManager.registerDatabase(database);

	        String URI = "xmldb:exist://localhost:8080/exist/xmlrpc/db/gimnasio";
	        
	        col = DatabaseManager.getCollection(URI, user, pass);
	        col.setProperty(OutputKeys.INDENT, "no");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void closeConnection() {
		try {
			col.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void menorSaldoDebe() {
		String query = "for $sucursal in doc('/db/gimnasio/sucursales.xml')//sucursal\n" + 
				"    return\n" + 
				"        <datos>\n" + 
				"            <menorSaldoDebe>{min($sucursal//saldodebe)}</menorSaldoDebe>\n" + 
				"            <cuentas>{$sucursal/cuenta[saldodebe = min($sucursal//saldodebe)]/numero}</cuentas>\n" + 
				"        </datos>";
		mostrar(query);
	}
	
	public void menorSaldoHaber() {
		String query = "let $clientes := doc('/db/gimnasio/clientes.xml')//clien\n" + 
				"\n" + 
				"for $sucursal in doc('/db/gimnasio/sucursales.xml')//sucursal\n" + 
				"    return\n" + 
				"        <datos>\n" + 
				"            {$clientes[@numero = $sucursal/cuenta[saldohaber = min($sucursal//saldohaber)]/cliente]/nombre}\n" + 
				"            <minimio>{min($sucursal//saldohaber)}</minimio>\n" + 
				"        </datos>";
		mostrar(query);
	}
	
	public void clientes() {
		String query = "<clientes>\n" + 
				"    {\n" + 
				"        for $cliente in doc('/db/gimnasio/clientes.xml')//clien\n" + 
				"            return\n" + 
				"                <cliente numero=\"{$cliente/@numero}\" nombre=\"{$cliente/nombre}\" telefono=\"{$cliente/tlf}\">\n" + 
				"                    {$cliente/poblacion}\n" + 
				"                    {$cliente/direccion}\n" + 
				"                </cliente>\n" + 
				"    }        \n" + 
				"</clientes>";
		mostrar(query);
	}
	
	public void modificarNodoCliente() {
		String query = "update rename doc('/db/gimnasio/sucursales.xml')//cliente as \"nombreCliente\"";
		ejecutar(query);
		System.out.println("NODO ACTUALIZADO");
	}
	
	public void addTitulo() {
		String query = "update insert\n" + 
				"    <titulo>Sucursales bancarias</titulo>\n" + 
				"preceding doc('/db/gimnasio/sucursales.xml')//sucursal";
		ejecutar(query);
		System.out.println("TÍTULO AÑADIDO");
	}
	
	public void mostrar(String query) {
		try {
            XQueryService xq = (XQueryService) col.getService("XQueryService", "1.0");
			xq.setProperty("indent", "yes");
			
	        // Query
	        CompiledExpression expression = xq.compile(query);
	        
	        // Ejecución
	        ResourceSet result = xq.execute(expression);
	        
	        // Recorrer
	        ResourceIterator iterator = result.getIterator();
	        
	        while(iterator.hasMoreResources()) {
	        	Resource res = iterator.nextResource();
	        	System.out.println(res.getContent());
	        }
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void ejecutar(String query) {
		try {
            XQueryService xq = (XQueryService) col.getService("XQueryService", "1.0");
			xq.setProperty("indent", "yes");
			
	        // Query
	        CompiledExpression expression = xq.compile(query);
	        
	        // Ejecución
	        ResourceSet result = xq.execute(expression);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
