import baseDatos.Operations;

public class Main {

	public static void main(String[] args) {
		Operations op = new Operations();
		
		op.openConnection();
		
		op.menorSaldoDebe();
		System.out.println("-----------------------");
		op.menorSaldoHaber();
		System.out.println("-----------------------");
		op.clientes();
		System.out.println("-----------------------");
		op.modificarNodoCliente();
		System.out.println("-----------------------");
		op.addTitulo();
		
		op.closeConnection();
	}

}
