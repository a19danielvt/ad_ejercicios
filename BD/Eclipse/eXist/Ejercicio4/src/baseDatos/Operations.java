package baseDatos;

import javax.xml.transform.OutputKeys;

import org.xmldb.api.DatabaseManager;
import org.xmldb.api.base.Collection;
import org.xmldb.api.base.CompiledExpression;
import org.xmldb.api.base.Database;
import org.xmldb.api.base.Resource;
import org.xmldb.api.base.ResourceIterator;
import org.xmldb.api.base.ResourceSet;
import org.xmldb.api.base.XMLDBException;
import org.xmldb.api.modules.XQueryService;

public class Operations {
	String user = "admin";
	String pass = "abc123..";
	
	Collection col;
	
	public void openConnection() {
		try {
			String driver = "org.exist.xmldb.DatabaseImpl"; //Driver para eXist
			Class c1 = Class.forName(driver); //Carga o driver
			
			Database database = (Database) c1.newInstance();
			database.setProperty("create-database", "true");
	        DatabaseManager.registerDatabase(database);

	        String URI = "xmldb:exist://localhost:8080/exist/xmlrpc/db/GIMNASIO";
	        
	        col = DatabaseManager.getCollection(URI, user, pass);
	        col.setProperty(OutputKeys.INDENT, "no");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void closeConnection() {
		try {
			col.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void actividades_gimnasio_socios() {
		try {
            XQueryService xq = (XQueryService) col.getService("XQueryService", "1.0");
			xq.setProperty("indent", "yes");
			
	        // Query
	        CompiledExpression expression = xq.compile(
					"<ACTIVIDADES_GIM>\n" + 
					"    <actividades_totales>{count(doc('/db/GIMNASIO/actividades_gim.xml')//fila_actividades)}</actividades_totales>\n" + 
					"    {\n" + 
					"        for $actividad in doc('/db/GIMNASIO/actividades_gim.xml')//fila_actividades\n" + 
					"            return \n" + 
					"                <fila_actividades cod=\"{data($actividad/@cod)}\" tipo=\"{data($actividad/@tipo)}\">\n" + 
					"                    {$actividad/NOMBRE}\n" + 
					"                    <numero_socios>{count(distinct-values(doc('/db/GIMNASIO/uso_gimnasio.xml')//fila_uso[CODACTIV = $actividad/@cod]/CODSOCIO))}</numero_socios>\n" + 
					"                </fila_actividades>\n" + 
					"    }    \n" + 
					"</ACTIVIDADES_GIM>"
	        );
	        
	        // Ejecución
	        ResourceSet result = xq.execute(expression);
	        
	        // Recorrer
	        ResourceIterator iterator = result.getIterator();
	        
	        while(iterator.hasMoreResources()) {
	        	Resource res = iterator.nextResource();
	        	System.out.println(res.getContent());
	        }
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}


























