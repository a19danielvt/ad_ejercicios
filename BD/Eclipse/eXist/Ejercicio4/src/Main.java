import baseDatos.Operations;

public class Main {

	public static void main(String[] args) {
		Operations op = new Operations();
		
		op.openConnection();
		op.actividades_gimnasio_socios();
		op.closeConnection();
	}

}
