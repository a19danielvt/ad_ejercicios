package baseDatos;

import java.util.Scanner;

import javax.xml.transform.OutputKeys;

import org.xmldb.api.DatabaseManager;
import org.xmldb.api.base.Collection;
import org.xmldb.api.base.CompiledExpression;
import org.xmldb.api.base.Database;
import org.xmldb.api.base.Resource;
import org.xmldb.api.base.ResourceIterator;
import org.xmldb.api.base.ResourceSet;
import org.xmldb.api.base.XMLDBException;
import org.xmldb.api.modules.XQueryService;

public class Operations {
	
	static Collection col;
	Scanner sc = new Scanner(System.in);

	public void openConnection() {
		try {
			String driver = "org.exist.xmldb.DatabaseImpl"; //Driver para eXist
			Class c1 = Class.forName(driver); //Carga o driver
			
			Database database = (Database) c1.newInstance();
			database.setProperty("create-database", "true");
	        DatabaseManager.registerDatabase(database);
	        
	        String URI = "xmldb:exist://localhost:8080/exist/xmlrpc/db/gimnasio";
	        String user = "admin";
	        String pass = "abc123..";
	        
	        col = DatabaseManager.getCollection(URI, user, pass);
	        col.setProperty(OutputKeys.INDENT, "no");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void closeConnection() {
        try {
			col.close();
        }catch(Exception e) {
        	e.printStackTrace();
        }
	}
	
	public void showDepartments() {
        try {
        	System.out.println("DEPARTAMENTOS----------\n");
            XQueryService xq = (XQueryService) col.getService("XQueryService", "1.0");
			xq.setProperty("indent", "yes");
			
	        // Query
	        CompiledExpression expression = xq.compile(
	        		"for $dep in doc('/db/gimnasio/departamentos.xml')//filaDepar\n"
	        		+ "return $dep"
	        );
	        
	        // Ejecución
	        ResourceSet result = xq.execute(expression);
	        
	        // Recorrer
	        ResourceIterator iterator = result.getIterator();
	        
	        while(iterator.hasMoreResources()) {
	        	Resource res = iterator.nextResource();
	        	System.out.println(res.getContent());
	        }
		} catch (XMLDBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
        System.out.println();
	}
	
	public void insertDep() {
		System.out.println("INSERTAR DEPARTAMENTO----------\n");
		
		System.out.print("Número de departamento: "); String number = sc.next();
		System.out.print("Nombre del departamento: "); String name = sc.next();
		System.out.print("Localización del departamento: "); String loc = sc.next();
		
		Department dep = new Department(number, name, loc);
		
		if(existsDepartment(dep.getDept_no())) {
			System.err.println("\nEL DEPARTAMENTO YA EXISTE\n");
			return;
		}
		
		try {
			XQueryService xq = (XQueryService) col.getService("XQueryService", "1.0");
			xq.setProperty("indent", "yes");
			
			// Query
			CompiledExpression expression = xq.compile(
					"update insert\n"
					+ "<filaDepar>\n"
					+ "  <DEPT_NO>" + dep.getDept_no() + "</DEPT_NO>\n"
					+ "  <DNOMBRE>" + dep.getDnombre() + "</DNOMBRE>\n"
					+ "  <LOC>" + dep.getLoc() + "</LOC>\n"
					+ "</filaDepar>\n"
					+ "into doc('/db/gimnasio/departamentos.xml')/departamentos"
			);
			
			// Ejecución
			ResourceSet result = xq.execute(expression);
			
			System.out.println("\nDEPARTAMENTO INSERTADO\n");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void removeDep() {
		System.out.println("ELIMINAR DEPARTAMENTO----------\n");
		
		System.out.print("Introduce el número del departamento: "); String number = sc.next();
		
		if(!existsDepartment(number)) {
			System.out.println("\nEL DEPARTAMENTO NO EXISTE\n");
			return;
		}
		
		try {
			XQueryService xq = (XQueryService) col.getService("XQueryService", "1.0");
			xq.setProperty("indent", "yes");
			
			// Query
			CompiledExpression expression = xq.compile(
					"update delete doc('/db/gimnasio/departamentos.xml')//filaDepar[DEPT_NO = " + number + "]"
			);
			
			// Ejecución
			ResourceSet result = xq.execute(expression);
			
			System.out.println("\nDEPARTAMENTO ELIMINADO\n");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void modifyDep() {
		System.out.println("MODIFICAR DEPARTAMENTO----------\n");
		
		System.out.print("Introduce el número del departamento: "); String number = sc.next();
		
		if(!existsDepartment(number)) {
			System.out.println("\nEL DEPARTAMENTO NO EXISTE\n");
			return;
		}
		
		System.out.print("Nuevo nombre: "); String name = sc.next();
		System.out.print("Nueva localización: "); String loc = sc.next();
		
		try {
			XQueryService xq = (XQueryService) col.getService("XQueryService", "1.0");
			xq.setProperty("indent", "yes");
			
			// Query
			CompiledExpression expression = xq.compile(
					"for $dep in doc('/db/gimnasio/departamentos.xml')//filaDepar[DEPT_NO = " + number + "]\n"
					+ "    return\n"
					+ "        update replace $dep\n"
					+ "        with \n"
					+ "            <filaDepar>\n"
					+ "                <DEPT_NO>" + number + "</DEPT_NO>\n"
					+ "                <DNOMBRE>" + name + "</DNOMBRE>\n"
					+ "                <LOC>" + loc + "</LOC>\n"
					+ "            </filaDepar>"
			);
			
			// Ejecución
			ResourceSet result = xq.execute(expression);
			
			System.out.println("\nDEPARTAMENTO MODIFICADO\n");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public boolean existsDepartment(String number) {
		ResourceIterator iterator;
		
		try {
			XQueryService xq = (XQueryService) col.getService("XQueryService", "1.0");
			xq.setProperty("indent", "yes");
			
			// Query
			CompiledExpression expression = xq.compile(
					"for $dep in doc('/db/gimnasio/departamentos.xml')//filaDepar[DEPT_NO = "
					+ number + "]\n"
					+ "return $dep"
			);
			
			// Ejecución
			ResourceSet result = xq.execute(expression);
			
	        // Recorrer
	        iterator = result.getIterator();
	        
	        if(iterator.hasMoreResources()) return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return false;
	}
}
