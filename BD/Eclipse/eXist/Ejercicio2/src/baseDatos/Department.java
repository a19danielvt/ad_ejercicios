package baseDatos;

public class Department {
	private String dept_no;
	private String dnombre;
	private String loc;
	
	public Department(String dept_no, String dnombre, String loc) {
		super();
		this.dept_no = dept_no;
		this.dnombre = dnombre;
		this.loc = loc;
	}

	public String getDept_no() {
		return dept_no;
	}

	public void setDept_no(String dept_no) {
		this.dept_no = dept_no;
	}

	public String getDnombre() {
		return dnombre;
	}

	public void setDnombre(String dnombre) {
		this.dnombre = dnombre;
	}

	public String getLoc() {
		return loc;
	}

	public void setLoc(String loc) {
		this.loc = loc;
	}
}
