import baseDatos.Operations;

public class Main {

	public static void main(String[] args) {
		Operations op = new Operations();
		
		op.openConnection();

		op.showDepartments();
		op.insertDep();
		op.removeDep();
		op.modifyDep();
		
		op.closeConnection();
	}
}
