package database;

import java.util.*;
import javax.persistence.TypedQuery;
import org.hibernate.*;

public class ClientOperations {
	private Session session;
	private Transaction ts;
	
	public void openConnection() throws Exception{
		SessionFactory sesion = HibernateUtil.getSessionFactory();
		session = sesion.getCurrentSession();
		
		if(session.getTransaction().isActive()) ts = session.getTransaction();
		else ts = session.beginTransaction();
	}
	
	public Client getClient(int idClient) throws Exception{
		Client c = session.get(Client.class, idClient);
		
		if(c == null) throw new Exception("INVALID ID"); 
		
		return c;
	}
	
	public void addClient(Client c) throws Exception{
		session.save(c);
		ts.commit();
	}
	
	public void modifyClient(Client c) throws Exception{
		session.update(c);
		ts.commit();
	}
	
	public void deleteClient(int id) throws Exception{
		Client c = getClient(id);

		if(c.getLoans().isEmpty()) {
			session.delete(c);
			ts.commit();
		}
		else 
			throw new Exception("CANNOT REMOVE DE CLIENT: it has loans");
	}
	
	public List<Client> clientsList(){
		String sentencia = "from Client";
		
		TypedQuery<Client> datos = session.createQuery(sentencia, Client.class);
		
		List<Client> list = datos.getResultList();
		
		return list;
	}
	
	public void closeConnection() throws Exception{
		session.close();
	}
}
