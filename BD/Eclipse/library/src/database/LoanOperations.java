package database;

import java.sql.Date;
import java.util.*;

import javax.persistence.TypedQuery;

import org.hibernate.*;

public class LoanOperations {
	private Session session;
	private Transaction ts;
	
	public void openConnection() throws Exception{
		SessionFactory sesion = HibernateUtil.getSessionFactory();
		session = sesion.getCurrentSession();
		
		if(session.getTransaction().isActive()) ts = session.getTransaction();
		else ts = session.beginTransaction();
	}
	
	public void closeConnection() throws Exception{
		session.close();
	}
	
	public boolean isBorrowed(int id) throws Exception{	
		String sentencia = "select l from Loan l where l.book.idBook=?1 order by l.date desc";
		TypedQuery<Loan> datos = session.createQuery(sentencia, Loan.class);

		datos.setParameter(1, id);
		
		Loan l = datos.getResultList().get(0);
		
		if(l == null) throw new Exception("INVALID ID");
		
		if(l.getBorrowed()) return true;
		
		return false;
	}
	
	public void addLoan(int book, int client) throws Exception{
		if(!bookExists(book)) throw new Exception("INVALID BOOK");
		if(isBorrowed(book)) throw new Exception("BOOK BORROWED");
		if(!clientExists(client)) throw new Exception("INVALID CLIENT");
		
		ClientOperations co = new ClientOperations();
		
		Client c = getClient(client);
		Book b = getBook(book);
		
		Date date = new Date(System.currentTimeMillis());
		
		Loan l = new Loan(b, c, date, true);
		
		session.save(l);
		ts.commit();
	}
	
	public boolean bookExists(int id) {
		String sentencia = "select b from Book b where b.idBook=?1";
		TypedQuery<Book> datos = session.createQuery(sentencia, Book.class);
		
		datos.setParameter(1, id);
		
		if(datos.getResultList().isEmpty()) return false;
		
		return true;
	}
	
	public boolean clientExists(int id) {
		String sentencia = "select c from Client c where c.idClient=?1";
		TypedQuery<Client> datos = session.createQuery(sentencia, Client.class);
		
		datos.setParameter(1, id);
		
		if(datos.getResultList().isEmpty()) return false;
		
		return true;
	}
	
	public Client getClient(int idClient) throws Exception{
		Client c = session.get(Client.class, idClient);
		
		if(c == null) throw new Exception("INVALID ID"); 
		
		return c;
	}
	
	public Book getBook(int id) {
		String sentencia = "select b from Book b where b.idBook=?1";
		TypedQuery<Book> datos = session.createQuery(sentencia, Book.class);
		
		datos.setParameter(1, id);
		
		return datos.getSingleResult();
	}
	
	public void addReturn(int id) throws Exception{
		if(!bookExists(id)) throw new Exception("INVALID ID");
		if(!isBorrowed(id)) throw new Exception("BOOK NOT BORROWED");
		
		String sentencia = "select l from Loan l where l.book.idBook=?1 order by l.date desc";
		TypedQuery<Loan> datos = session.createQuery(sentencia, Loan.class);
		
		datos.setParameter(1, id);
		
		Loan l = datos.getResultList().get(0);
		
		l.setBorrowed(false);
		
		session.update(l);
		ts.commit();
	}
	
	public List<Book> borrowedBookList(){		
		String sentencia = "from Book";
		TypedQuery<Book> datos = session.createQuery(sentencia, Book.class);
		
		List<Book> all = datos.getResultList();
		
		sentencia = "select l.borrowed from Loan l where l.book.idBook=?1 order by l.date desc";
		TypedQuery<Boolean> bool = session.createQuery(sentencia, Boolean.class);
		
		List<Book> borrowed = new ArrayList<>();
		for(Object o : all) {
			Book b = (Book) o;
			bool.setParameter(1, b.getIdBook());
			
			if(bool.getResultList().get(0)) borrowed.add(b);
		}
		
		return borrowed;
	}
}
