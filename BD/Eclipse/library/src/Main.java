import database.*;

import java.util.*;

public class Main {
	
	static Scanner sc = new Scanner(System.in);
	static ClientOperations co = new ClientOperations();
	static LoanOperations lo = new LoanOperations();
	
	public static void main(String[] args) {
		try {
			//co.openConnection();
			//lo.openConnection();
			
			while(true) {
				
				System.out.println("\n1. Add client.");
				System.out.println("2. Get client.");
				System.out.println("3. Modify client.");
				System.out.println("4. Delete client.");
				System.out.println("5. Clients list.");
				System.out.println("6. Book borrowed.");
				System.out.println("7. Add loan.");
				System.out.println("8. Add return.");
				System.out.println("9. Borrowed books.");
				System.out.println("Other -> Close.");
				
				System.out.print("Option: "); int option = sc.nextInt();
				System.out.println();
				
				if(option > 0 & option < 6) co.openConnection();
				else if(option >= 6 & option < 10) lo.openConnection();
				
				if(option < 1 | option > 9) {
					System.out.println("BYE");
					break;
				}
				
				switch(option) {
				case 1:
					addClient();
					break;
				case 2:
					getClient();
					break;
				case 3:
					modifyClient();
					break;
				case 4:
					deleteClient();
					break;
				case 5:
					clientsList();
					break;
				case 6:
					isBorrowed();
					break;
				case 7:
					addLoan();
					break;
				case 8:
					addReturn();
					break;
				case 9:
					borrowedBooks();
					break;
				}
			}
			co.closeConnection();
			lo.closeConnection();
			
		}catch(Exception e) {
			System.out.println("Error: " + e.getMessage());
		}
	}
	
	public static void addClient() {
		try {
			System.out.print("DNI: "); String dni = sc.next(); sc.nextLine();
			System.out.print("Name: "); String name = sc.nextLine();
			System.out.print("Email: "); String email = sc.next();
			System.out.println();
			
			co.addClient(new Client(dni, name, email));
			
			System.out.println("CLIENT ADDED");
		}catch(Exception e) {
			System.out.println("Error: " + e.getMessage());
		}
	}
	
	public static void getClient() {
		try {
			System.out.print("Introduce a client id: "); int id = sc.nextInt();
			
			Client c = co.getClient(id);
			System.out.println("Name: " + c.getName());
			System.out.println("Loans:");
			for(Object o : c.getLoans()) {
				Loan l = (Loan) o;
				System.out.println("\t- " + l.getBook().getTitle() + ", " + l.getDate() + ", " + 
						(l.getBorrowed()? "borrowed" : "available"));
			}
		}catch(Exception ex) {
			System.out.println("Error: " + ex.getMessage());
		}
	}
	
	public static void modifyClient() {
		try {
			System.out.print("Id of the client: "); int id = sc.nextInt();
			System.out.print("New DNI: "); String dni = sc.next(); sc.nextLine();
			System.out.print("New Name: "); String name = sc.nextLine();
			System.out.print("New email: "); String email = sc.next();
			System.out.println();
			
			co.modifyClient(new Client(id, dni, name, email));
			
			System.out.println("CLIENT MODIFIED");
		}catch(Exception e) {
			System.out.println("Error: " + e.getMessage());
		}
	}
	
	public static void deleteClient() {
		try {
			System.out.print("id: "); int id = sc.nextInt();
			System.out.println();
			
			co.deleteClient(id);
			
			System.out.println("CLIENT DELETED");
		}catch(Exception e) {
			System.out.println("Error: " + e.getMessage());
		}
	}
	
	public static void clientsList() {
		try {
			List<Client> list = co.clientsList();
			
			for(Client c : list) {
				System.out.println(c);
			}
		}catch(Exception e) {
			System.out.println("Error: " + e.getMessage());
		}
	}
	
	public static void isBorrowed() {
		try {
			System.out.print("Introduce a book id: "); int id = sc.nextInt();
			System.out.println();
			
			System.out.println(lo.isBorrowed(id)? "It is borrowed" : "It is available");
		}catch(Exception e) {
			System.out.println("Error: " + e.getMessage());
		}
	}
	
	public static void addLoan() {
		try {
			System.out.print("Book id: "); int book = sc.nextInt();
			System.out.print("Client id: "); int client = sc.nextInt();
			System.out.println();
			
			lo.addLoan(book, client);
			
			System.out.println("LOAN ADDED");
		}catch(Exception e) {
			System.out.println("Error: " + e.getMessage());
		}
	}
	
	public static void addReturn() {
		try {
			System.out.print("Book id: "); int id = sc.nextInt();
			System.out.println();
			
			lo.addReturn(id);
			
			System.out.println("RETURN ADDED");
		}catch(Exception e) {
			System.out.println("Error: " + e.getMessage());
		}
	}
	
	public static void borrowedBooks() {
		try {
			List<Book> list = lo.borrowedBookList();
			
			for(Book b : list) {
				System.out.println(b);
			}
		}catch(Exception e) {
			System.out.println("Error: " + e.getMessage());
		}
	}
}
