import java.util.*;
import clases.*;

public class Main {
	public static void main(String[] args) {
		Operations op = new Operations();
		
		try {
			op.openConnection();
			
			List<Student> lista = op.studentsList();
			for(Student s : lista)
				System.out.println(s);
			
//			Student s = new Student("22222222B", "Nombre", "Apellido", 34);
//			op.addStudent(s);
		
//			op.deleteStudent("22222222B");
		
//			op.modifyStudent("22222222B");
			
			System.out.println("Mi alumno: " + op.getStudent("11111111A"));
			
			op.closeConnection();
	
		}catch(Exception e) {
			System.out.println("Error: " + e.getMessage());
		}
	}
}
