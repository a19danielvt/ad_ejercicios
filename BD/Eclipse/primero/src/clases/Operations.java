package clases;

import java.util.*;
import org.hibernate.*;
import javax.persistence.*;

public class Operations {
	private Session session;
	
	public void openConnection() {
		SessionFactory sesion = HibernateUtil.getSessionFactory();
		session = sesion.openSession();
	}
	
	public void addStudent(Student s) {
		Transaction ts = session.beginTransaction();
		session.save(s);
		ts.commit();
	}
	
	public Student getStudent(String dni) {
		TypedQuery<Student> query1 = session.createQuery("select s from Student s where s.dni=?1", Student.class);
		query1.setParameter(1, dni);
		Student student = query1.getSingleResult();
		
		return student;
	}
	
	public void modifyStudent(String dni) {
		Transaction ts = session.beginTransaction();
		
		//Obtener dao
		TypedQuery<Student> query = session.createQuery("select s from Student s where s.dni=?1", Student.class);
		query.setParameter(1, dni);
		Student student = query.getSingleResult();
		
		//Modificar
		student.setName("Alba");
		student.setSurname("Agulla");
		student.setAge(30);
		
		session.update(student);
		ts.commit(); 
	}
	
	public void deleteStudent(String dni) {
		Transaction ts = session.beginTransaction();
		
		//Obtener dato
		TypedQuery<Student> query = session.createQuery("select s from Student s where s.dni=?1", Student.class);
		query.setParameter(1, dni);
		Student student = query.getSingleResult();
		
		//Borrar student
		session.delete(student);
		ts.commit();
	}
	
	public List<Student> studentsList(){
		TypedQuery<Student> lista = session.createQuery("from Student", Student.class);
		List<Student> list = lista.getResultList();
		
		return list;
	}
	
	public void closeConnection() {
		session.close();
	}
}
