import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Scanner;

public class Prueba {
	public static void main(String[] args) {
		try {
			Scanner sc = new Scanner(System.in);
			while(true) {
				System.out.print("Introduce una palabra: ");
				String palabra = sc.next();
				
				if(palabra.equals("fin")) break;
			
				String[] comandos = {"java", "-jar", "Otro.jar", palabra};
				ProcessBuilder pb = new ProcessBuilder(comandos);
				Process p = pb.start();
				
				InputStream is = p.getInputStream();
				BufferedReader bf = new BufferedReader(new InputStreamReader(is));
				
				String linea;
				while((linea = bf.readLine()) != null) {
					System.out.println(linea);
				}
				
				is.close();
				bf.close();
			}
			
			sc.close();
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}
	}
}
