package clases;

import java.util.*;
import org.hibernate.*;

public class Operations {
	
	private Session session;
	private Transaction ts;
	
	public void openConnection() throws Exception{
		SessionFactory factory = HibernateUtil.getSessionFactory();
		session = factory.getCurrentSession();
		
		if (session.getTransaction().isActive()) ts = session.getTransaction();
		else ts = session.beginTransaction();
	}
	
	public void closeConnection() throws Exception{
		session.close();
	}
	
	public List<Facturas> listaFacturas() throws Exception{
		return session.createQuery("from Facturas", Facturas.class).getResultList();
	}
	
	public void engadeFactura(Facturas f) throws Exception{
		session.save(f);
		ts.commit();
	}
	
	public void modificaFactura(Facturas f) throws Exception{
		session.update(f);
		ts.commit();
	}
	
	public void borraFactura(int id) throws Exception{
		session.delete(getFactura(id));
		ts.commit();
	}
	
	public Facturas getFactura(int id) throws Exception{
		Facturas f = session.get(Facturas.class, id);
		if (f == null) throw new Exception("INVALID ID");
		return f;
	}
	
	public Clientes getCliente(int id) throws Exception{
		Clientes c = session.get(Clientes.class, id);
		if (c == null) throw new Exception("INVALID ID");
		return c;
	}
}
