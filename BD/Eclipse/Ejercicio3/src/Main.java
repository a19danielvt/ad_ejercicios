import java.util.*;
import clases.*;

public class Main {

	private static Operations o = new Operations();
	private static Scanner sc = new Scanner(System.in);
	
	public static void main(String[] args) {
		try {
			main:
			while (true) {
				o.openConnection();
				
				System.out.println("1. Lista de facturas");
				System.out.println("2. Añadir factura.");
				System.out.println("3. Borrar factura.");
				System.out.println("4. Modificar factura.");
				System.out.println("Otro para salir");
				
				System.out.print("Introduce un número: ");int option = sc.nextInt();
				System.out.println();
				
				switch(option) {
				case 1:
					listaFacturas();
					break;
				case 2:
					engadeFactura();
					break;
				case 3:
					borraFactura();
					break;
				case 4:
					modificaFactura();
					break;
				default:
					break main;
				}
				
				System.out.println();
			}
			
			System.out.println("BYE");
			
			o.closeConnection();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	public static void listaFacturas() throws Exception{
		List<Facturas> lista = o.listaFacturas();

		for (Facturas f : lista) {
			System.out.println("ID: " + f.getIdFactura());
			Clientes cliente = f.getClientes();
			System.out.println("\tCLIENTE: \n\t\tNOMBRE: " + cliente.getNome() + 
					"\n\t\tDIRECCIÓN: " + cliente.getEnderezo() +
					"\n\t\tTELÉFONOS: ");
			for (Telefonos t : cliente.getTelefonoses()) {
				System.out.println("\t\t\t" + t.getDescricion() + ": " + t.getNumero());
			}
			System.out.println("\tDETALLE: " + f.getDetalle() + "\n\tIMPORTE: " + 
					f.getImporte() + "\n");
		}
	}
	
	public static void engadeFactura() throws Exception{
		Clientes cliente = null;
		while(true) {
			try {
				System.out.print("Id del cliente: "); int id = sc.nextInt();
				cliente = o.getCliente(id);
				break;
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		}
		sc.nextLine();
		System.out.print("Introduce algún detalle: "); String detalle = sc.nextLine();
		System.out.print("Introduce el importe: "); double importe = sc.nextDouble();
		
		o.engadeFactura(new Facturas(cliente, detalle, importe));
		
		System.out.println("FACTURA AÑADIDA");
	}
	
	public static void modificaFactura() throws Exception{
		Facturas factura = null;
		while(true) {
			try {
				if (factura == null) {
					System.out.print("Id de la factura: "); int id = sc.nextInt();
					factura = o.getFactura(id);
				}
				System.out.print("Id del nuevo cliente: ");
				factura.setClientes(o.getCliente(sc.nextInt()));
				System.out.print("Nuevos detalles: "); sc.nextLine();
				factura.setDetalle(sc.nextLine());
				System.out.print("Importe: "); 
				factura.setImporte(sc.nextDouble());
				break;
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		}
		
		o.modificaFactura(factura);
		System.out.println("FACTURA MODIFICADA");
	}

	public static void borraFactura() {
		try {
			System.out.print("Introduce el id de la factura: "); int id = sc.nextInt();
			o.borraFactura(id);
			System.out.println("FACTURA BORRADA");
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
}
