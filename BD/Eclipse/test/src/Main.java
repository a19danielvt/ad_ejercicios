import java.util.*;
import clases.*;

public class Main {
	private static Operations o = new Operations();
	private static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		try {
			main:
			while(true) {
				o.openConnection();
				
				System.out.println("1. Book list.");
				System.out.println("2. Get book.");
				System.out.println("3. Add book.");
				System.out.println("4. Delete book.");
				System.out.println("5. Modify book.");
				System.out.println("Other to exit");
				
				System.out.print("Option: "); int option = sc.nextInt();
				System.out.println();
				
				switch(option) {
				case 1:
					bookList();
					break;
				case 2:
					getLibro();
					break;
				case 3:
					addLibro();
					break;
				case 4:
					deleteLibro();
					break;
				case 5:
					modifyLibro();
					break;
				default:
					System.out.println("BYE");
					break main;
				}
				System.out.println();
			}
			
			sc.close();
			o.closeConnection();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void bookList() {
		try {
			List<Libro> libros = o.bookList();
			libros.forEach(System.out::println);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void getLibro() {
		try {
			System.out.print("Enter the id of the book: "); int id = sc.nextInt();
			System.out.println(o.getLibro(id));
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	public static void addLibro() {
		System.out.print("ISBN: "); String isbn = sc.next(); sc.nextLine();
		System.out.print("Title: "); String title = sc.nextLine(); 
		System.out.print("Authors: "); String authors = sc.nextLine();
		System.out.print("Year: "); int year = sc.nextInt();
		System.out.println();
		
		try {
			o.addLibro(new Libro(isbn, title, authors, year));
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		System.out.println("BOOK ADDED SUCCESFULLY");
	}
	
	public static void deleteLibro() {
		System.out.print("Enter the id of the book: "); int id = sc.nextInt();
		try {
			o.deleteLibro(id);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		System.out.println("BOOK DELETED SUCCESFULLY");
	}
	
	public static void modifyLibro() {
		Libro libro = null;
		while(true) {			
			System.out.print("Enter the id of the book: "); int id = sc.nextInt();
			try {
				libro = o.getLibro(id);
				break;
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		}
		
		System.out.print("New ISBN: "); libro.setIsbn(sc.next()); sc.nextLine();
		System.out.print("New title: "); libro.setTitulo(sc.nextLine());
		System.out.print("New authors: "); libro.setAutores(sc.nextLine());
		System.out.print("New year: "); libro.setAno(sc.nextInt());
		System.out.println();
		
		try {
			o.modifyLibro(libro);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		System.out.println("BOOK MODIFIED SUCCESFULLY");
	}
}
