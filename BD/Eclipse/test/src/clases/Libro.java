package clases;
// Generated 10 ene. 2021 13:17:45 by Hibernate Tools 5.2.12.Final

/**
 * Libro generated by hbm2java
 */
public class Libro implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private Integer idLibro;
	private String isbn;
	private String titulo;
	private String autores;
	private Integer ano;

	public Libro() {
	}

	public Libro(String isbn, String titulo, String autores, Integer ano) {
		this.isbn = isbn;
		this.titulo = titulo;
		this.autores = autores;
		this.ano = ano;
	}

	public Integer getIdLibro() {
		return this.idLibro;
	}

	public void setIdLibro(Integer idLibro) {
		this.idLibro = idLibro;
	}

	public String getIsbn() {
		return this.isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getTitulo() {
		return this.titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getAutores() {
		return this.autores;
	}

	public void setAutores(String autores) {
		this.autores = autores;
	}

	public Integer getAno() {
		return this.ano;
	}

	public void setAno(Integer ano) {
		this.ano = ano;
	}

	@Override
	public String toString() {
		return "Libro [idLibro=" + idLibro + ", isbn=" + isbn + ", titulo=" + titulo + ", autores=" + autores + ", ano="
				+ ano + "]";
	}

}
