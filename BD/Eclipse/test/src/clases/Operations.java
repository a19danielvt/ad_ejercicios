package clases;

import java.util.*;
import org.hibernate.*;

public class Operations {
	private Session session;
	private Transaction t;
	
	public void openConnection() throws Exception{
		SessionFactory factory = HibernateUtil.getSessionFactory();
		session = factory.openSession();
		
		if (session.getTransaction().isActive()) t = session.getTransaction();
		else t = session.beginTransaction();
	}
	
	public void closeConnection() throws Exception{
		session.close();
	}
	
	public List<Libro> bookList() throws Exception{
		return session.createQuery("from Libro", Libro.class).getResultList();
	}
	
	public Libro getLibro(int id) throws Exception{
		Libro libro = session.get(Libro.class, id);
		
		if (libro == null) throw new Exception("INVALID ID");
		
		return libro;
	}
	
	public void addLibro(Libro libro) throws Exception{
		session.save(libro);
		t.commit();
	}
	
	public void deleteLibro(int id) throws Exception{
		session.delete(getLibro(id));
		t.commit();
	}
	
	public void modifyLibro(Libro libro) throws Exception{
		session.update(libro);
		t.commit();
	}
}
