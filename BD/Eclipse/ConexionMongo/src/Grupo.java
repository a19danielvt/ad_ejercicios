import java.util.ArrayList;

import org.bson.Document;

public class Grupo {
	private int id;
	private String grupo;
	private String regimen;
	private ArrayList<Alumno> alumnos;
	
	public Grupo(int id, String grupo, String regimen, ArrayList<Alumno> alumnos) {
		super();
		this.id = id;
		this.grupo = grupo;
		this.regimen = regimen;
		this.alumnos = alumnos;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getGrupo() {
		return grupo;
	}

	public void setGrupo(String grupo) {
		this.grupo = grupo;
	}

	public String getRegimen() {
		return regimen;
	}

	public void setRegimen(String regimen) {
		this.regimen = regimen;
	}

	public ArrayList<Alumno> getAlumnos() {
		return alumnos;
	}

	public void setAlumnos(ArrayList<Alumno> alumnos) {
		this.alumnos = alumnos;
	}
	
	public Document toDocument() {
		ArrayList<Document> alumnosDoc = new ArrayList<>();
		
		for(Alumno alumno : alumnos) {
			alumnosDoc.add(alumno.toDocument());
		}
		
		Document doc = new Document()
				.append("id", id)
				.append("grupo", grupo)
				.append("regimen", regimen)
				.append("alumnos", alumnosDoc);
		
		return doc;
	}
}
