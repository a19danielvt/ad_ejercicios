import org.bson.Document;
import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        try {
    	    Operations op = new Operations();
    	    op.openConnection();
    	    
//    	    op.createCollection("grupos");
    	    
    	    Alumno alumno1 = new Alumno("11111111A", "Daniel", "Vieites", 20);
    	    Alumno alumno2 = new Alumno("11112222B", "Carlos", "Castro", 25);
    	    Alumno alumno3 = new Alumno("11113333C", "Daniel", "Vieites", 20);
    	    
    	    ArrayList<Alumno> alumnos1 = new ArrayList<>();
    	    alumnos1.add(alumno1);
    	    alumnos1.add(alumno2);

    	    ArrayList<Alumno> alumnos2 = new ArrayList<>();
    	    alumnos2.add(alumno3);
    	    alumnos2.add(alumno2);
    	    
    	    Grupo grupo1 = new Grupo(1, "grupo1", "regimen1", alumnos1);
    	    Grupo grupo2 = new Grupo(2, "grupo2", "regimen2", alumnos2);
    	    
    	    //insertar datos
//    	    ArrayList<Document> grupos = new ArrayList<>();
//    	    grupos.add(grupo1.toDocument());
//    	    grupos.add(grupo2.toDocument());
//    	    op.insertMany("grupos", grupos);
    	    
    	    //mostrar datos de los grupos
    	    ArrayList<Document> docs = op.find("grupos");
    	    for(Document doc : docs) {
    	    	System.out.println(doc.get("id"));
    	    	System.out.println(doc.get("grupo"));
    	    	System.out.println(doc.get("regimen"));
    	    	
    	    	ArrayList<Document> alumnos = (ArrayList<Document>)doc.get("alumnos");
    	    	for(Document alumno : alumnos) {
    	    		System.out.println("\t" + alumno.get("dni"));
    	    		System.out.println("\t" + alumno.get("nombre"));
    	    		System.out.println("\t" + alumno.get("apellidos"));
    	    		System.out.println("\t" + alumno.get("edad"));
    	    		System.out.println();
    	    	}
    	    	
    	    	System.out.println();
    	    }
    	    
    	    
            op.closeConnection();
            System.out.println("Funciona");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
	}

}