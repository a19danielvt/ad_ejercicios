import com.mongodb.*;
import com.mongodb.client.*;
import com.mongodb.client.model.Sorts;
import org.bson.Document;
import static com.mongodb.client.model.Filters.*;
import com.mongodb.client.result.*;
import static com.mongodb.client.model.Updates.*;
import java.util.ArrayList;

public class Operations {
	
	public MongoDatabase db;
	private MongoClient mongoClient;
	
	public void openConnection() throws Exception{
        mongoClient = new MongoClient("192.168.1.114");
        db = mongoClient.getDatabase("midb");
	}
	
	public void closeConnection() {
		mongoClient.close();
	}
	
	public void createCollection(String name) throws Exception{
		db.createCollection(name);
	}
	
	public void insertOne(String collection, Document doc) throws Exception{
	    db.getCollection(collection).insertOne(doc);
	}
	
	public void insertMany(String collection, ArrayList<Document> docs) throws Exception{
		db.getCollection(collection).insertMany(docs);
	}
	
	public Document getDocument(String collection, String field, Object value) throws Exception{
		Document doc = db.getCollection(collection).find(eq(field, value)).first();
		if (doc == null) throw new Exception("No existe el documento");
		return doc;
	}
	
	public long modifyDocument(String collection, String field, Object value, String fieldMod, 
			Object newValue) throws Exception{
		return db.getCollection(collection).updateOne(eq(field, value), set(fieldMod, newValue))
				.getModifiedCount();
	}
	
	public long deleteDocument(String collection, String field, Object value) {
		return db.getCollection(collection).deleteMany(eq(field, value)).getDeletedCount();
	}
	
	public ArrayList<Document> find(String collection) throws Exception{
		ArrayList<Document> docs = db.getCollection(collection).find()
        		.into(new ArrayList<Document>());
		
		if(docs.size() == 0) throw new Exception("No existe la colección");
		
		return docs;
	}
}
