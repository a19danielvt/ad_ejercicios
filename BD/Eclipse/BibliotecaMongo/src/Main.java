import java.util.ArrayList;

import org.bson.Document;

import baseDatos.Operaciones;
import baseDatos.Prestamo;
import baseDatos.Cliente;
import baseDatos.Direccion;
import baseDatos.Libro;
import test.TestCliente;
import test.TestPrestamo;

public class Main {

	public static void main(String[] args) {
		try {
			Operaciones op = new Operaciones();
			TestCliente cliente = new TestCliente(op.getDB());
			TestPrestamo prestamo = new TestPrestamo(op.getDB());
			
			// direcciones
			Direccion dir1 = new Direccion("calle1", "1", "Santiago", "A Corunha");
			Direccion dir2 = new Direccion("calle2", "2", "Santiago", "A Corunha");
			Direccion dir3 = new Direccion("calle3", "3", "Santiago", "A Corunha");

			ArrayList<Direccion> direcciones1 = new ArrayList<>();
			direcciones1.add(dir1);
			direcciones1.add(dir2);
			
			ArrayList<Direccion> direcciones2 = new ArrayList<>();
			direcciones2.add(dir2);
			direcciones2.add(dir3);
			
			
			// clientes
			Cliente c1 = new Cliente("11111111A", "Daniel", "Vieites", direcciones1);
			Cliente c2 = new Cliente("22222222B", "Manuel", "Vieites", direcciones2);
			
			
			// libros
			Libro l1 = new Libro("isbn1", "Titulo1", new String[]{"Autor1", "Autor2"});
			Libro l2 = new Libro("isbn2", "Titulo2", new String[]{"Autor3", "Autor4"});
			
			
			// prestamos
			Prestamo p1 = new Prestamo("28/01/2021", c1, l1);
			Prestamo p2 = new Prestamo("01/02/2021", c2, l2);
			
			System.out.println("Crea la colección préstamos");
			op.createCollection("prestamos");
			
			System.out.println("Inserta préstamos");
			prestamo.insertPrestamo(p1.toDocument(), "prestamos");
			prestamo.insertPrestamo(p2.toDocument(), "prestamos");
			
			System.out.println("Modifica un préstamo que tiene el cliente con dni 11111111A");
			System.out.println(prestamo.modifyPrestamo("prestamos", "cliente.dni", "11111111A", "libro", l2.toDocument()));

			System.out.println("Todos los préstamos");
			System.out.println(prestamo.findAll("prestamos").size());
			
			System.out.println("Borra el cliente con nombre Daniel");
			System.out.println(cliente.deleteClient("prestamos", "nombre", "Daniel"));

			System.out.println("Inserta de nuevo el cliente");
			System.out.println(cliente.insertClient(c1.toDocument(), "prestamos", "fecha", "28/01/2021"));
			
			System.out.println("Modifica el cliente cambiándole el nombre");
			System.out.println(cliente.modifyClient("prestamos", "nombre", "Daniel", "nombre", "Carlos"));
			
			System.out.println("Cliente con nombre Carlos");
			System.out.println(cliente.getClient("prestamos", "nombre", "Carlos"));
			
			System.out.println("Pone el libro 1 en el primer préstamo");
			System.out.println(prestamo.modifyPrestamo("prestamos", "fecha", "28/01/2021", "libro", l1.toDocument()));
			
			System.out.println("Borra los préstamos que tengan el libro 2");
			System.out.println(prestamo.deletePrestamo("prestamos", "libro.titulo", "Titulo2"));

			System.out.println("funciona");
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

}
