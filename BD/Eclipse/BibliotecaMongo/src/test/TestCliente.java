package test;

import java.util.ArrayList;

import org.bson.Document;
import org.bson.conversions.Bson;

import baseDatos.Cliente;
import baseDatos.Direccion;

import com.mongodb.client.MongoDatabase;
import static com.mongodb.client.model.Filters.*;
import static com.mongodb.client.model.Updates.*;

public class TestCliente {
	
	private MongoDatabase db;
	
	public TestCliente(MongoDatabase db) {
		this.db = db;
	}
	
	public long insertClient(Document doc, String collection, String field, String value) {
		return db.getCollection(collection)
					.updateOne(eq(field, value), set("cliente", doc))
					.getModifiedCount();
	}
	
	public long deleteClient(String collection, String clientField, Object value) {
		return db.getCollection(collection)
					.updateMany(eq("cliente." + clientField, value), unset("cliente"))
					.getModifiedCount();
	}
	
	public long modifyClient(String collection, String clientField, Object value, String modClientField, Object modValue) {
		return db.getCollection(collection)
					.updateMany(eq("cliente." + clientField, value), set("cliente." + modClientField, modValue))
					.getMatchedCount();
	}
	
	public Cliente getClient(String collection, String clientField, Object value) throws Exception{
		Document doc = db.getCollection(collection).find(eq("cliente." + clientField, value)).first();
		
		if(doc == null) throw new Exception("No existe ese cliente");
		
		Document cliente = (Document) doc.get("cliente");
		
		String dni = cliente.getString("dni");
		String nombre = cliente.getString("nombre");
		String apellidos = cliente.getString("apellidos");
		
		ArrayList<Document> direccionesDoc = (ArrayList<Document>)cliente.get("direcciones");
		ArrayList<Direccion> direcciones = new ArrayList<>();
		for(Document direccion : direccionesDoc) {
			String calle = direccion.getString("calle");
			String numero = direccion.getString("numero");
			String localidad = direccion.getString("localidad");
			String provincia = direccion.getString("provincia");
			
			direcciones.add(new Direccion(calle, numero, localidad, provincia));
		}
		
		return new Cliente(dni, nombre, apellidos, direcciones);
	}
	
	public ArrayList<Cliente> findAll(String collection){
		ArrayList<Document> docs = db.getCollection(collection).find().into(new ArrayList<>());
		
		ArrayList<Cliente> clientes = new ArrayList<>();
		ArrayList<String> dnis = new ArrayList<>();
		for (Document doc : docs) {
			Document clienteDoc = (Document) doc.get("cliente");
			
			String dni = clienteDoc.getString("dni");
			if (dnis.contains(dni)) continue;
			else dnis.add(dni);
			
			String nombre = clienteDoc.getString("nombre");
			String apellidos = clienteDoc.getString("apellidos");
			ArrayList<Document> direccionesDoc = (ArrayList<Document>) clienteDoc.get("direcciones");
			ArrayList<Direccion> direcciones = new ArrayList<>();
			for(Document direccion : direccionesDoc) {
				String calle = direccion.getString("calle");
				String numero = direccion.getString("numero");
				String localidad = direccion.getString("localidad");
				String provincia = direccion.getString("provincia");
				
				direcciones.add(new Direccion(calle, numero, localidad, provincia));
			}
			
			clientes.add(new Cliente(dni, nombre, apellidos, direcciones));
		}
		
		return clientes;
	}
}
