package test;

import java.util.ArrayList;

import org.bson.Document;

import com.mongodb.client.MongoDatabase;

import baseDatos.Cliente;
import baseDatos.Direccion;
import baseDatos.Libro;
import baseDatos.Prestamo;
import jdk.nashorn.api.tree.ForInLoopTree;

import static com.mongodb.client.model.Filters.*;
import static com.mongodb.client.model.Updates.*;

public class TestPrestamo {

	private MongoDatabase db;
	
	public TestPrestamo(MongoDatabase db) {
		this.db = db;
	}
	
	public void insertPrestamo(Document doc, String collection) throws Exception{
		db.getCollection(collection).insertOne(doc);
	}
	
	public void insertPrestamo(ArrayList<Document> docs, String collection) {
		db.getCollection(collection).insertMany(docs);
	}
	
	public long deletePrestamo(String collection, String field, Object value){
		return db.getCollection(collection)
					.deleteMany(eq(field, value))
					.getDeletedCount();
	}
	
	public long modifyPrestamo(String collection, String field, Object value, String modField, Object modValue) {
		return db.getCollection(collection)
					.updateOne(eq(field, value), set(modField, modValue))
					.getModifiedCount();
	}
	
	public ArrayList<Prestamo> findAll(String collection){
		ArrayList<Document> docs = db.getCollection(collection).find().into(new ArrayList<Document>());
		
		ArrayList<Prestamo> prestamos = new ArrayList<>();
		for (Document prestamo : docs) {
			// fecha
			String fecha = prestamo.getString("fecha");
			
			// cliente
			Document clienteDoc = (Document) prestamo.get("cliente");
			String dni = clienteDoc.getString("dni");
			String nombre = clienteDoc.getString("nombre");
			String apellidos = clienteDoc.getString("apellidos");
			ArrayList<Document> direccionesDoc = (ArrayList<Document>)clienteDoc.get("direcciones");
			ArrayList<Direccion> direcciones = new ArrayList<>();
			for (Document direccion : direccionesDoc) {
				String calle = direccion.getString("calle");
				String numero = direccion.getString("numero");
				String localidad = direccion.getString("localidad");
				String provincia = direccion.getString("provincia");
				
				direcciones.add(new Direccion(calle, numero, localidad, provincia));
			}
			Cliente cliente = new Cliente(dni, nombre, apellidos, direcciones);
			
			// libro
			Document libroDoc = (Document) prestamo.get("libro");
			String isbn = libroDoc.getString("isbn");
			String titulo = libroDoc.getString("titulo");
			ArrayList<String> autoresDoc = (ArrayList<String>) libroDoc.get("autores");
			String[] autores = new String[autoresDoc.size()];
			for (int i = 0; i < autores.length; i++) {
				autores[i] = autoresDoc.get(i);
			}
			Libro libro = new Libro(isbn, titulo, autores);
			
			prestamos.add(new Prestamo(fecha, cliente, libro));
		}
		
		return prestamos;
	}
}
