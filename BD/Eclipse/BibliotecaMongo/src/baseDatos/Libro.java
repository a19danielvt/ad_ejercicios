package baseDatos;

import java.util.ArrayList;
import java.util.Arrays;

import org.bson.Document;

public class Libro {
	private String isbn;
	private String titulo;
	private String[] autores;
	
	public Libro(String isbn, String titulo, String[] autores) {
		this.isbn = isbn;
		this.titulo = titulo;
		this.autores = autores;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String[] getAutores() {
		return autores;
	}

	public void setAutores(String[] autores) {
		this.autores = autores;
	}

	@Override
	public String toString() {
		return "Libro [isbn=" + isbn + ", titulo=" + titulo + ", autores=" + Arrays.toString(autores) + "]";
	}
	
	public Document toDocument() {
		ArrayList<String> autores = new ArrayList<>(Arrays.asList(this.autores));
		
		return new Document()
				.append("isbn", isbn)
				.append("titulo", titulo)
				.append("autores", autores);
	}
}
