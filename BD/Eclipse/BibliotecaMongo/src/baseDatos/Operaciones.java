package baseDatos;

import java.util.ArrayList;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;
import static com.mongodb.client.model.Filters.*;

public class Operaciones {
	
	private MongoClient client;
	private MongoDatabase db;
	
	public Operaciones() {
		openConnection();
	}
	
	private void openConnection() {
		client = new MongoClient("192.168.56.101");
		db = client.getDatabase("biblioteca");
	}
	
	public void closeConnection() throws Exception{
		client.close();
	}
	
	public void createCollection(String collection) throws Exception{
		db.createCollection(collection);
	}
	
	public MongoDatabase getDB() {
		return db;
	}
}
