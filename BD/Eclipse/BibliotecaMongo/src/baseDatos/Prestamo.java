package baseDatos;

import org.bson.Document;

public class Prestamo {
	private String fecha;
	private Cliente cliente;
	private Libro libro;
	
	public Prestamo(String fecha, Cliente cliente, Libro libro) {
		this.fecha = fecha;
		this.cliente = cliente;
		this.libro = libro;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Libro getLibro() {
		return libro;
	}

	public void setLibro(Libro libro) {
		this.libro = libro;
	}

	@Override
	public String toString() {
		return "Prestamo [fecha=" + fecha + ", cliente=" + cliente + 
				", libro=" + libro + "]";
	}
	
	public Document toDocument() {
		return new Document()
				.append("fecha", fecha)
				.append("cliente", cliente.toDocument())
				.append("libro", libro.toDocument());
	}
}
