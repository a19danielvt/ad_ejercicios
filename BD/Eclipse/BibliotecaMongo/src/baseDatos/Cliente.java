package baseDatos;

import java.util.ArrayList;

import org.bson.Document;

public class Cliente {
	private String dni;
	private String nombre;
	private String apellidos;
	private ArrayList<Direccion> direcciones;
	
	public Cliente(String dni, String nombre, String apellidos, ArrayList<Direccion> direcciones) {
		this.dni = dni;
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.direcciones = direcciones;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public ArrayList<Direccion> getDirecciones() {
		return direcciones;
	}

	public void setDirecciones(ArrayList<Direccion> direcciones) {
		this.direcciones = direcciones;
	}

	@Override
	public String toString() {
		return "Cliente [dni=" + dni + ", nombre=" + nombre + ", apellidos=" + apellidos + ", direcciones="
				+ direcciones + "]";
	}
	
	public Document toDocument() {
		ArrayList<Document> direcciones = new ArrayList<>();
		
		for(Direccion dir : this.direcciones) {
			direcciones.add(dir.toDocument());
		}
		
		return new Document()
				.append("dni", dni)
				.append("nombre", nombre)
				.append("apellidos", apellidos)
				.append("direcciones", direcciones);
	}
}
