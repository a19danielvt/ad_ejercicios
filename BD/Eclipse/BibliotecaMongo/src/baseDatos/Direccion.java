package baseDatos;

import org.bson.Document;

public class Direccion {
	private String calle;
	private String numero;
	private String localidad;
	private String provincia;
	
	public Direccion(String calle, String numero, String localidad, String provincia) {
		this.calle = calle;
		this.numero = numero;
		this.localidad = localidad;
		this.provincia = provincia;
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getLocalidad() {
		return localidad;
	}

	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}

	public String getProvincia() {
		return provincia;
	}

	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}

	@Override
	public String toString() {
		return "Direccion [calle=" + calle + ", numero=" + numero + ", localidad=" + localidad + ", provincia="
				+ provincia + "]";
	}
	
	public Document toDocument() {
		return new Document()
				.append("calle", calle)
				.append("numero", numero)
				.append("localidad", localidad)
				.append("provincia", provincia);
	}
}
