import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Scanner;

import dataBase.*;

/**
 * Program
 */
public class Program {
    public static void main(String[] args) {
        Operations op = new Operations();

        try {
            op.openConnection();

            Scanner sc = new Scanner(System.in);

            System.out.print("Introduce un id de cliente: ");
            int idClient = sc.nextInt();

            if(op.existsClient(idClient))
                System.out.println("El cliente existe");
            else
                System.out.println("El cliente no existe");


            System.out.print("Introduce un id de libro: ");
            int idBook = sc.nextInt();

            if(op.existsClient(idBook)){
                System.out.println("El libro existe");
                System.out.print("Introduce el código del libro: ");

                String code = sc.next();
                if(op.isBorrowed(code))
                    System.out.println("Está prestado");
                else
                    System.out.println("No está prestado");
            }
            else
                System.out.println("El libro no existe");

            //Descomentar para probar
            //op.addLoan("asdfdsa", 2);

            //Descomentar para probar
            //op.addReturn("asdfdsa");

            System.out.println("");
            System.out.println("Libros prestados");
            ArrayList<Loan> list = op.borrowedBooksList();
            for(Loan l : list)
                System.out.println(l);

            System.out.println("\n");

            System.out.println("Libros disponibles");
            list = op.availableBooksList();
            for(Loan l : list)
                System.out.println(l);

            op.closeConnection();
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }
}