package dataBase;

/**
 * Client
 */
public class Client {
    private int idClient;
    private String dni;
    private String name;
    private String email;

    public Client(int idClient, String dni, String name, String email) {
        this.idClient = idClient;
        this.dni = dni;
        this.name = name;
        this.email = email;
    }

    public int getIdClient() {
        return idClient;
    }

    public void setIdClient(int idClient) {
        this.idClient = idClient;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String toString(){
        return idClient + ", " + dni + ", " + name + ", " + email;
    }
}