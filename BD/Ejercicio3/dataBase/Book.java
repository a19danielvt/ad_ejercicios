package dataBase;

/**
 * Book
 */
public class Book {
    private int idBook;
    private String code;
    private String title;
    private String authors;
    private int year;

    public Book(int idBook, String code, String title, String authors, int year) {
        this.idBook = idBook;
        this.code = code;
        this.title = title;
        this.authors = authors;
        this.year = year;
    }

    public int getIdBook() {
        return idBook;
    }

    public void setIdBook(int idBook) {
        this.idBook = idBook;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthors() {
        return authors;
    }

    public void setAuthors(String authors) {
        this.authors = authors;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String toString(){
        return idBook + ", " + code + ", " + title + ", " + authors + ", " + year;
    }
}