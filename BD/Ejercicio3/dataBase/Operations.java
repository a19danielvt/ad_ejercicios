package dataBase;

import java.sql.*;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Operations
 */
public class Operations {
    private Connection conn;

    public void openConnection() throws Exception{
        Class.forName("com.mysql.jdbc.Driver");
        String url = "jdbc:mysql://192.168.56.101/library";
        conn = DriverManager.getConnection(url, "libraryManager", "abc123.");
    }

    public void closeConnection() throws Exception{
        conn.close();
    }

    public boolean existsClient(int idClient) throws Exception{
        String sentencia = "select * from client where idClient=?";
        PreparedStatement consulta = conn.prepareStatement(sentencia);
        consulta.setInt(1, idClient);
        ResultSet datos = consulta.executeQuery();

        if(datos.next())
            return true;
        
        return false;
    }

    public boolean existsBook(int idBook) throws Exception{
        String sentencia = "select * from book where idBook=?";
        PreparedStatement consulta = conn.prepareStatement(sentencia);
        consulta.setInt(1, idBook);
        ResultSet datos = consulta.executeQuery();

        if(datos.next())
            return true;
        
        return false;
    }

    public boolean isBorrowed(String code) throws Exception{
        String sentencia = "select borrowed from loan where idBook=(" +
            "select idBook from book where code=?" +
            ") order by date desc";

        PreparedStatement consulta = conn.prepareStatement(sentencia);
        consulta.setString(1, code);
        ResultSet datos = consulta.executeQuery();

        int valor = -1;
        if(datos.next()){
            valor = datos.getInt(1);

            if(valor == 0)
                return false;
            
            return true;
        }
        throw new Exception("El código no existe");
    }

    public void addLoan(String cod, int idClient) throws Exception{
        String sentencia = "select idBook from book where code=?";
        PreparedStatement consulta = conn.prepareStatement(sentencia);
        consulta.setString(1, cod);
        ResultSet datos = consulta.executeQuery();

        if(!datos.next())
            throw new Exception("No existe ese código");
        
        int idBook = datos.getInt(1);

        if(!existsClient(idClient))
            throw new Exception("No existe ese cliente");

        sentencia = "insert into loan(idBook, idClient, date, borrowed) values (?, ?, ?, ?)";
        consulta = conn.prepareStatement(sentencia);
        
        consulta.setInt(1, idBook);
        consulta.setInt(2, idClient);
        consulta.setDate(3, new Date(Calendar.getInstance().getTime().getTime()));
        consulta.setInt(4, 1);

        consulta.executeUpdate();
    }

    public void addReturn(String code) throws Exception{
        String sentencia = "select max(date) from loan where idBook=(select idBook from book " +
        "where code=?)";
        PreparedStatement consulta = conn.prepareStatement(sentencia);
        consulta.setString(1, code);
        ResultSet datos = consulta.executeQuery();
        
        if(!datos.next())
            throw new Exception("No existe ese código.");
        
        Date fecha = datos.getDate(1);

        sentencia = "update loan set borrowed=? where idBook=(select idBook from book where " +
            "code=?) and date=?";
        consulta = conn.prepareStatement(sentencia);
        
        int borrowed = -1;
        if(isBorrowed(code))
            borrowed = 0;
        else
            borrowed = 1;
        
        consulta.setInt(1, borrowed);
        consulta.setString(2, code);
        consulta.setDate(3, fecha);

        consulta.executeUpdate();
    }   

    public ArrayList<Loan> borrowedBooksList() throws Exception{
        ArrayList<Loan> list = new ArrayList<>();

        for(int i = 1; i < 4; i++){
            
            String sentencia = "select code from book where idBook=?";
            PreparedStatement consulta = conn.prepareStatement(sentencia);
            consulta.setInt(1, i);
            ResultSet datos = consulta.executeQuery();
            
            String code = "";
            if(datos.next())
                code = datos.getString(1);

            if(!isBorrowed(code))
                continue;

            sentencia = "select * from book where idBook=?";
            consulta = conn.prepareStatement(sentencia);
            consulta.setInt(1, i);
            datos = consulta.executeQuery();

            Book book = null;
            while(datos.next()){
                int idBook = datos.getInt(1);
                String bookCode = datos.getString(2);
                String title = datos.getString(3);
                String authors = datos.getString(4);
                int year = datos.getInt(5);

                book = new Book(idBook, bookCode, title, authors, year);
            }


            sentencia = "select idClient from loan where idBook=? order by date desc";
            consulta = conn.prepareStatement(sentencia);
            consulta.setInt(1, i);
            datos = consulta.executeQuery();
            
            int idClient = -1;
            if(datos.next())
                idClient = datos.getInt(1);

            sentencia = "select * from client where idClient=?";
            consulta = conn.prepareStatement(sentencia);
            consulta.setInt(1, idClient);
            datos = consulta.executeQuery();

            Client client = null;
            while(datos.next()){
                int idCliente = datos.getInt(1);
                String dni = datos.getString(2);
                String name = datos.getString(3);
                String email = datos.getString(4);

                client = new Client(idCliente, dni, name, email);
            }

            sentencia = "select * from loan where idBook=? order by date desc";
            consulta = conn.prepareStatement(sentencia);
            consulta.setInt(1, i);
            datos = consulta.executeQuery();

            Loan loan = null;
            if(datos.next()){
                int idLoan = datos.getInt(1);
                Date dateLoan = datos.getDate(4);
                boolean borrowed = datos.getBoolean(5);
            
                loan = new Loan(idLoan, book, client, dateLoan, borrowed);
            }

            list.add(loan);
        }

        return list;
    }

    public ArrayList<Loan> availableBooksList() throws Exception{
        ArrayList<Loan> list = new ArrayList<>();

        ArrayList<Loan> compartidos = borrowedBooksList();

        int[] idsNoCompartidos = {1, 2, 3};
        
        for(Loan l : compartidos){
            if(l.getBook().getIdBook() == 1)
                idsNoCompartidos[0] = -1;
            else if(l.getBook().getIdBook() == 2)
                idsNoCompartidos[1] = -1;
            else if(l.getBook().getIdBook() == 3)
                idsNoCompartidos[2] = -1;
        }

        for(int i = 0; i < idsNoCompartidos.length; i++){
            if(idsNoCompartidos[i] == -1)
                continue;

            String sentencia = "select * from book where idBook=?";
            PreparedStatement consulta = conn.prepareStatement(sentencia);
            consulta.setInt(1, idsNoCompartidos[i]);
            ResultSet datos = consulta.executeQuery();
            
            Book book = null;
            if(datos.next()){
                int idBook = datos.getInt(1);
                String code = datos.getString(2);
                String title = datos.getString(3);
                String authors = datos.getString(4);
                int year = datos.getInt(5);

                book = new Book(idBook, code, title, authors, year);
            }

            sentencia = "select * from loan where idBook=? order by date desc";
            consulta = conn.prepareStatement(sentencia);
            consulta.setInt(1, idsNoCompartidos[i]);
            datos = consulta.executeQuery();

            Loan loan = null;
            if(datos.next()){
                int idLoan = datos.getInt(1);
                int idClient = datos.getInt(3);

                sentencia = "select * from client where idClient=?";
                consulta = conn.prepareStatement(sentencia);
                consulta.setInt(1, idClient);
                ResultSet datosCliente = consulta.executeQuery();

                Client client = null;
                if(datosCliente.next()){
                    int idCliente = datosCliente.getInt(1);
                    String dni = datosCliente.getString(2);
                    String name = datosCliente.getString(3);
                    String email = datosCliente.getString(4);

                    client = new Client(idCliente, dni, name, email);
                }

                Date date = datos.getDate(4);
                boolean borrowed = datos.getBoolean(5);

                loan = new Loan(idLoan, book, client, date, borrowed);
            }

            list.add(loan);
        }

        return list;
    }
}