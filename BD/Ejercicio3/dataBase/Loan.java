package dataBase;

import java.sql.Date;

/**
 * Loan
 */
public class Loan {
    private int idLoan;
    private Book book;
    private Client client;
    private Date date;
    private boolean borrowed;

    public Loan(int idLoan, Book book, Client client, Date date, boolean borrowed) {
        this.idLoan = idLoan;
        this.book = book;
        this.client = client;
        this.date = date;
        this.borrowed = borrowed;
    }

    public int getIdLoan() {
        return idLoan;
    }

    public void setIdLoan(int idLoan) {
        this.idLoan = idLoan;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public boolean isBorrowed() {
        return borrowed;
    }

    public void setBorrowed(boolean borrowed) {
        this.borrowed = borrowed;
    }
    
    public String toString() {
        return idLoan + ", " + book.getIdBook() + ", " + client.getIdClient() + ", "
            + date + ", " + (borrowed? 1 : 0);
    }
}