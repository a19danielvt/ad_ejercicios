import java.sql.*;
public class Conexion{
  public static void main(String[] args){
    try{
      //Carga do controlador JDBC de MySQL
      Class.forName("com.mysql.jdbc.Driver");
      //Crea unha conexion a base de datos
      String url="jdbc:mysql://192.168.56.101/exercises";
      Connection conexion = DriverManager.getConnection(url,"manager","abc123.");

      //Operamos coa conexion
      //Mostrar a version do driver JDBC
      DatabaseMetaData meta = conexion.getMetaData();
      System.out.println("A version do driver JDBC e: "+meta.getDriverVersion());
 
      //Consulta de todos los datos de la tabla alumno
      /*Statement s = conexion.createStatement();
      ResultSet datos = s.executeQuery("select * from student");

      while(datos.next()){
	   String dni = datos.getString("dni");
	   String nombre = datos.getString(2);
	   String apellido = datos.getString(3);
	   int edad = datos.getInt(4);
	   System.out.println("dni: " + dni + ", nombre: " + nombre
		+ ", apellido: " + apellido + ", edad: " + edad);
      }
      datos.close();

      Consulta con parámetros*/
      String cadena = "select * from student where DNI=?";
      PreparedStatement consulta = conexion.prepareStatement(cadena);

      consulta.setString(1, "22222222B");

      ResultSet datos = consulta.executeQuery();
      if(datos.next()){
	   System.out.println("datos: " + datos.getString("name"));
      }

      //Añadir datos
      String dni = "66666777E";
      String nombre = "Dani";
      String apellido = "Vieites";
      int edad = 19;

      String cadenaDatos = "insert into student(dni, name, surname, age) values (?, ?, ?, ?)";

      PreparedStatement añadir = conexion.prepareStatement(cadenaDatos);
      añadir.setString(1, dni);
      añadir.setString(2, nombre);
      añadir.setString(3, apellido);
      añadir.setInt(4, edad);

      int filasAñadidas = añadir.executeUpdate();

      System.out.println("filas añadidas: " + filasAñadidas);

      //Cerrar conexion
      conexion.close();
    }
    catch (SQLException erro){
      System.out.println("Erro SQL: "+erro.getMessage());
      erro.printStackTrace();
    }
    catch (Exception erro){
      System.out.println("Erro: "+erro.getMessage());
      erro.printStackTrace();
    }
  }//main
}//class
