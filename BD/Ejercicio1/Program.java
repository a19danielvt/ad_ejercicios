import java.util.ArrayList;
import java.util.Scanner;

import operations.*;

public class Program {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        OperationsDB op = new OperationsDB();
        ArrayList<Student> students = null;

        try{
            op.openConnection();
        }catch(Exception e){
            System.out.println("Fallo al conectarse a la base de datos: " + e.getMessage());
        }
        /*
        try {
            students = op.studentsList();            
        } catch (Exception e) {
            System.out.println("Fallo al obtener la lista actual de alumnos: " + e.getMessage());
        }

        try {
            mostrarAlumnos(op);
        } catch (Exception e) {
            System.out.println("Fallo al leer los alumnos: " + e.getMessage());
        }

        try{
            /**
             * Añadir alumno
             
            System.out.println("AÑADIR ALUMNO----------");
            
            System.out.print("DNI del nuevo alumno: "); String nuevoDni = sc.next();
            System.out.print("Nombre del nuevo alumno: "); String nuevoNombre = sc.next();
            System.out.print("Apellido del nuevo alumno: "); String nuevoApellido = sc.next();
            System.out.print("Edad del nuevo alumno: "); int nuevaEdOperationsDB op ad = sc.nextInt();

            Student st = new Student(nuevoDni, nuevoNombre, nuevoApellido, nuevaEdad);
            op.addStudent(st);

        }catch(Exception e){
            System.out.println("Fallo al añadir alumno: " + e.getMessage());
        }

        try{
            /**
             * Modificar alumno


            System.out.println("MODIFICAR ALUMNO----------\n");
            String d = confirmar(students);

            System.out.println();

            System.out.print("Nuevo nombre: "); String nombre = sc.next();
            System.out.print("Nuevo apellido: "); String apellido = sc.next();
            System.out.print("Nueva edad: "); int edad = sc.nextInt();

            

            Student student = new Student(d, nombre, apellido, edad);

            op.modifyStudent(student);

            System.out.println("Alumno modificado\n");
        }catch(Exception e){
            System.out.println("Fallo al modificar el alumno: " + e.getMessage());
        }

        try{
            /**
             * Obtener alumno
            
            System.out.println("OBTENER DATOS DE ALUMNO----------\n");
            
            String obtenerDni = confirmar(students);

            Student obtenido = op.getStudent(obtenerDni);
  
            System.out.println(String.format("%-12s %-15s %-20s %-3s\n", "dni", "nombre", "apellido", "edad"));
            System.out.println(obtenido);
            

        }catch(Exception e){
            System.out.println("Fallo: " + e.getMessage());
        }
        
        try{
            /**
             * Eliminar alumno
            
            String eliminarDni = confirmar(students);

            op.deleteStudent(eliminarDni);

            System.out.println("El alumno a sido eliminado\n");
             

        }catch(Exception e){
            System.out.println("Fallo al eliminar el alumno: " + e.getMessage());
        }
*/
        try{
            op.closeConnection();
        }catch(Exception e){
            System.out.println("Fallo al cerrar la conexión: " + e.getMessage());
        }

        sc.close();
    }

    public static void mostrarAlumnos(OperationsDB op) throws Exception{
        System.out.println("ALUMNOS ACTUALES----------");

        System.out.println(String.format("%-12s %-15s %-20s %-3s\n", "dni", "nombre", "apellido", "edad"));

        ArrayList<Student> students = op.studentsList();

        for(Student s : students){
            System.out.println(s);
        }

        System.out.println("\n");
    }

    public static String confirmar(ArrayList<Student> students){
        Scanner sc = new Scanner(System.in);
        String dni = "";
        while(true){
            boolean existe = false;
            System.out.print("Introduce el dni del alumno: "); dni = sc.next();

            for(Student s : students){
                if(s.getDni().equals(dni))
                    existe = true;
            }

            if(!existe)
                System.out.println("El dni introducido no es válido\n");
            else
                break;
        }

        return dni;
    }
}