1. Usando el codumento universida.xml, devuelve el nombr de departamento encerrado entre las etiquetas <tipoA></tipoA>, si es del tipo=A, y <tipoB></tipoB>, si no lo es.

```
for $dep in doc("/db/gimnasio/universidad.xml")//departamento
    return 
        if ($dep/@tipo = "A") then 
            <tipoA>{data($dep/nombre)}</tipoA>
        else 
            <tipoB>{data($dep/nombre)}</tipoB>
```

2. Usando el documento universidad.xml, obtén los nombres de departamentos, el número de empleados que tiene y la media del salario entre etiquetas.

```
for $dep in doc("/db/gimnasio/universidad.xml")//departamento
    return 
        <depart>
            {$dep/nombre}
            <numeroEmple>{count($dep/empleado)}</numeroEmple>
            <mediaSalario>{avg($dep/empleado/@salario)}</mediaSalario>
        </depart>
```

3. Empleando EMPLEADOS.xml, obtén:

- Los nombres de oficio que empiezan por P.

```
for $oficio in doc("/db/gimnasio/empleados.xml")//OFICIO
    return
        if(matches($oficio, "^P")) then 
            data($oficio)
        else 
            ()
```

- Los nombres de oficio con número de empleados de cada oficio. Utiliza la función distinct-values.

```
let $doc := doc("db/gimnasio/empleados.xml")

for $ofi in distinct-values($doc//OFICIO)
    return 
        <dato>
            <oficio>{data($ofi)}</oficio>
            <empleados>{count($doc//EMP_ROW[OFICIO = $ofi])}</empleados>
        </dato>
```

- El número de empleados que tiene cada departamento y la media de salario redondeada.

```
for $distinct in distinct-values(doc("db/gimnasio/empleados.xml")//DEPT_NO)
    let $empleados := doc("db/gimnasio/empleados.xml")//EMP_ROW[DEPT_NO = $distinct]
    return 
        <dato>
            <empleados>{count($empleados)}</empleados>
            <media>{round-half-to-even(avg($empleados/SALARIO), 2)}</media>
        </dato>
```

1. Utilizando el documento productos.xml, realiza las siguientes consultas Xquery:

- Obtén por cada zona el número de productos que tiene.

```
for $distinct in distinct-values(doc("db/gimnasio/productos.xml")//cod_zona)
    return 
        <dato>
            <zona>{data($distinct)}</zona>
            <productos>{count(doc("db/gimnasio/productos.xml")//produc[cod_zona = $distinct])}</productos>
        </dato>
```

- Obtén la denominación de los productos entre las etiquetas <zona10></zona10> si son del código de zona 10, <zona20></zona20> si son de la zona 20, <zona30></zona30> si son de la 30 y <zona40></zona40> si son de la 40.

```
for $producto in doc("db/gimnasio/productos.xml")//produc
    let $deno := $producto/denominacion/text()
    return
        if($producto/cod_zona = "10") then
            <zona10>{$deno}</zona10>
        else if ($producto/cod_zona = "20") then
            <zona20>{$deno}</zona20>
        else if ($producto/cod_zona = "30") then
            <zona30>{$deno}</zona30>
        else if ($producto/cod_zona = "40") then
            <zona40>{$deno}</zona40>
        else
            ()
```

- Obtén por cada zona la denominación del o de los productos más caros.

```
for $zona in distinct-values(doc("db/gimnasio/productos.xml")//cod_zona)
    let $producto := doc("db/gimnasio/productos.xml")//produc[cod_zona = $zona]
    return
        $producto/denominacion[../precio = max($producto/precio)]
```

- Obtén la denominación de los productos contenida entre las etiquetas <placa></placa> para los productos que tengan en la denominación la palabra "Placa Base", <memoria></memoria>, para los que contienen la palabra "Memoria", <micro></micro> para los que contienen la palabra "Micro", y <otro></otro> para el resto de productos.

```
for $producto in doc("db/gimnasio/productos.xml")//produc
    let $deno := $producto/denominacion
    return
        if (matches($deno, "^Placa Base")) then
            <placa>{data($deno)}</placa>
        else if (matches($deno, "^Memoria")) then
            <memoria>{data($deno)}</memoria>
        else if (matches($deno, "^Micro")) then
            <micro>{data($deno)}</micro>
        else
            <otro>{data($deno)}</otro>
```

6. Utilizando el documento sucursais.xml. Realiza las siguientes consultsa Xquery:

- Devuelve el código de sucursal y el número de cuentas que tiene de tipo AHORRO y de tipo PENSIONES.

```
for $suc in doc("db/gimnasio/sucursales.xml")//sucursal
    let $cuenta := $suc/cuenta
    return
        <sucursal>
            {$suc/@codigo}
            <ahorro>{count($cuenta[@tipo = "AHORRO"])}</ahorro>
            <pensiones>{count($cuenta[@tipo = "PENSIONES"])}</pensiones>
        </sucursal>
```

- Devuelve por cada sucursal el código de sucursal, el director, la población, la suma del total debe y la suma del total haber de sus cuentas.

```
for $suc in doc("db/gimnasio/sucursales.xml")//sucursal
    return
        <sucursal>
            {$suc/@codigo}
            {$suc/director}
            {$suc/poblacion}
            <totaldebe>{sum($suc/cuenta/saldodebe)}</totaldebe>
            <totalhaber>{sum($suc/cuenta/saldohaber)}</totalhaber>
        </sucursal>
```

- Devuelve el nombre de los directores, el código de sucursal y la población de las sucursales con más de 3 cuentas.

```
for $suc in doc("db/gimnasio/sucursales.xml")//sucursal
    return
        if(count($suc/cuenta) > 3) then
            <datos>
                {$suc/@codigo}
                {$suc/director}
                {$suc/poblacion}
            </datos>
        else 
            ()
```

(joins de documentos)

4. Visualizar por cada empleado del documento empleados.xml su apellido, su número de departamento y el nombre del departamento que se encuentra en el documento departamentos.xml

```
for $emp in doc("db/gimnasio/empleados.xml")//EMP_ROW,
    $dep in doc("db/gimnasio/departamentos.xml")//DEP_ROW[DEPT_NO = $emp/DEPT_NO]
    return
        <empleado>
            <apellido>{data($emp/APELLIDO)}</apellido>
            <departamento>
                <numero>{data($emp/DEPT_NO)}</numero>
                <nombre>{data($dep/DNOMBRE)}</nombre>
            </departamento>
        </empleado>
```

5. Utilizando los documentos departamentos.xml y empleados.xml, obtener por cada departamento, el nombre de departamento, el número de empleados y el salario total

```
for $dep in doc("db/gimnasio/departamentos.xml")//DEP_ROW
    let $emp := doc("db/gimnasio/empleados.xml")//EMP_ROW[DEPT_NO = $dep/DEPT_NO]
    return 
        <departamento>
            <nombre>{data($dep/DNOMBRE)}</nombre>
            <empleados>{count($emp)}</empleados>
            <salariototal>{sum($emp/SALARIO)}</salariototal>
        </departamento>
```

6. Transformar la salida de la consulta anterior de manera que el salario total y el número de empleados sean atributos de cada departamento.

```
for $dep in doc("db/gimnasio/departamentos.xml")//DEP_ROW
    let $emp := doc("db/gimnasio/empleados.xml")//EMP_ROW[DEPT_NO = $dep/DEPT_NO]
    let $salariototal := sum($emp/SALARIO)
    let $empleados := count($emp)
    return 
        <departamento empleados="{$empleados}" salariototal="{$salariototal}">
            <nombre>{data($dep/DNOMBRE)}</nombre>
        </departamento>
```

7. Utilizando los documentos departamentos.xml y empleados.xml, obtener por cada departamento el nombre de empleado que más gana.

```
for $dep in doc("db/gimnasio/departamentos.xml")//DEP_ROW
    let $emp := doc("db/gimnasio/empleados.xml")//EMP_ROW[DEPT_NO = $dep/DEPT_NO][SALARIO = max(SALARIO)]
    return 
        <departamento nombre="{$dep/DNOMBRE}">
            <empleado>{data($emp/APELLIDO)}</empleado>
        </departamento>
```

(varios for)

8. Visualiza por cada departamento del documento universidad.xml, el número de empleados que hay en cada puesto de trabajo.

```
for $dep in $doc//departamento
    return 
        <departamento nombre="{data($dep/nombre)}">
        {
            for $puesto in distinct-values($dep//puesto)
                return 
                    <puesto nombre="{data($puesto)}">
                        <empleados>{count($dep/empleado[puesto = $puesto])}</empleados>
                    </puesto>
        }
        </departamento>
```

9. Visualiza por cada departamento del documento universidad.xml, el salario ḿaximo y el empleado que tiene ese salario. Usar un primer for que obtiene los nodos departamento y un segundo for los empleados de cada departamento.

```
let $doc := doc("db/gimnasio/universidad.xml")

for $dep in $doc//departamento
    return
        <departamento>
            {
                for $emp in $dep/empleado
                    return
                        if ($emp/@salario = max($dep//@salario)) then
                            <empleado nombre="{$emp/nombre}" salario="{$emp/@salario}"/>
                        else
                            ()
            }
        </departamento>
```