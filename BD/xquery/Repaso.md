Escribe y prueba en el eXide los xqueries que se describen a continuación:

1. Muestra por pantalla por cada sucursal el menor saldo debe y el número de la cuenta que tiene ese saldo.

```
for $sucursal in doc('/db/gimnasio/sucursales.xml')//sucursal
    return
        <datos>
            <menorSaldoDebe>{min($sucursal//saldodebe)}</menorSaldoDebe>
            <cuentas>{$sucursal/cuenta[saldodebe = min($sucursal//saldodebe)]/numero}</cuentas>
        </datos>
```

2. Muestra por pantalla por cada sucursal el nombre del cliente de la cuenta del tipo AHORRO tal que su saldo haber sea el mínimo. Saca también el mínimo.

```
let $clientes := doc('/db/gimnasio/clientes.xml')//clien

for $sucursal in doc('/db/gimnasio/sucursales.xml')//sucursal
    return
        <datos>
            {$clientes[@numero = $sucursal/cuenta[saldohaber = min($sucursal//saldohaber)]/cliente]/nombre}
            <minimio>{min($sucursal//saldohaber)}</minimio>
        </datos>
```

3. Muestra por pantalla el contenido del fichero clientes.xml pero con el siguiente formato:
   
    <clientes>
        <cliente numero="20" nombre="Antonio García" telefono="927233445">
            <poblacion>Cáceres</poblacion>
            <direccion>Avda de Madrid 2</direccion>
        </cliente>
    </clientes>

```
<clientes>
    {
        for $cliente in doc('/db/gimnasio/clientes.xml')//clien
            return
                <cliente numero="{$cliente/@numero}" nombre="{$cliente/nombre}" telefono="{$cliente/tlf}">
                    {$cliente/poblacion}
                    {$cliente/direccion}
                </cliente>
    }        
</clientes>
```

4. Modifica el documento sucursales.xml, de modo que en lugar del elemento "<cliente>xx</cliente>" esté un elemento: <nombreCliente>nombre del cliente</nombreCliente>.

```
update rename doc('/db/gimnasio/sucursales.xml')//cliente as "nombreCliente"
```

5. Modifica el anterior documento añadiéndole un nodo con el siguiente contenido: <titulo>Sucursales bancarias.</titulo> como se muestra a continuación:

    <sucursales>
        <titulo>Sucursales bancarias.</titulo>
        <sucursal>...</sucursal>
        ...
    </sucursales>

```
update insert
    <titulo>Sucursales bancarias</titulo>
preceding doc('/db/gimnasio/sucursales.xml')//sucursal
```

6. Escribe un programa en Java con métodos que realicen las mismas acciones con los xqueries que acabas de escribir.