## Visualización de documentos con nodos nuevos, eliminación de nodos y nodos modificados

1. Muestra por pantalla el documento productos.xml en el que se añadió un producto con los datos: cod_prod: 1023, denominación: HD externo Seagate expansión 250 GB, precio: 70, stock_actual: 100, stock_minimo: 20, y el código de zona tiene que ser el código de zona correspondiente a Andalucía.

```
let $productos := doc("db/gimnasio/productos.xml")//produc
let $zonas := doc("db/gimnasio/zonas.xml")//zona

return 
    <productos>
        {doc("db/gimnasio/productos.xml")//TITULO}
        {$productos}
        <produc>
            <cod_prod>1023</cod_prod>
            <denominacion>HD externo Seagate expansión 250 GB</denominacion>
            <precio>70</precio>
            <stock_actual>100</stock_actual>
            <stock_minimo>20</stock_minimo>
            {$zonas/cod_zona[../nombre = "Andalucía"]}
        </produc>
    </productos>
```

2. Escribe el código xquery que muestra como resultado un elemento raíz <productos>, un título: <TITULO>DATOS DE LOS PRODUCTOS DE LA ZONA 30</TITULO>, y todos los elementos <produc> con código de zona igual a 30

```
<productos>
    <TITULO>DATOS DE LOS PRODUCTOS DE LA ZONA 30</TITULO>
    {doc("db/gimnasio/productos.xml")//produc[cod_zona = 30]}
</productos>
```

3. Crea una consulta que visualice todo el documento de productos.xml pero con los siguientes cambios:

   - Sube el precio de cada producto un 3%
   - Añade 10 unidades a los stock actual de todos los productos
   - Muestra el nombre de la zona en lugar del código de la zona

```
let $productos := doc("db/gimnasio/productos.xml")//produc

return
    <productos>
        {doc("db/gimnasio/productos.xml")//TITULO}
        {for $p in $productos
            return
                <prod>
                    {$p/cod_prod}
                    {$p/denominacion}
                    <precio>{$p/precio * 1.03}</precio>
                    <stock_actual>{$p/stock_actual + 10}</stock_actual>
                    <zona>{data(doc("db/gimnasio/zonas.xml")//zona/nombre[../cod_zona = $p/cod_zona])}</zona>
                </prod>
        }
    </productos>
```

## Sentencias de actualización de eXist

4. Modifica el documento universidad.xml, introduce los siguientes cambios:
   1. Añade un empleado al departamento que ocupa la posición 2. Los datos son el salario 2340, el puesto Técnico y nombre Pedro Fraile

```
update insert
<empleado salario="2340">
    <puesto>Técnico</puesto>
    <nombre>Pedro Fraile</nombre>
</empleado>
following doc("db/gimnasio/universidad.xml")/universidad/departamento[2]
```

   2. Actualiza el salario de los empleados del departamento con el código MAT1. Súmalle 100 al salario.

```
for $salario in doc("db/gimnasio/universidad.xml")//departamento[codigo = "MAT1"]/empleado/@salario
    return
        update value $salario with $salario + 100
```

   3. Renombra el nodo DEP_ROW del documento departamentos.xml por filaDepar.

```
update rename doc("db/gimnasio/departamentos.xml")//DEP_ROW as "filaDepar"
```

5. Modifica el documentto productos.xml, introduce los siguientes cambios:
   1. Sube el precio de cada producto un 3%
   
```
for $precio in doc("db/gimnasio/productos.xml")//produc/precio
    return
        update value $precio with $precio * 1.03
```

   2. Añade 10 unidades a los stock_actual de todos los productos

```
for $actual in doc("db/gimnasio/productos.xml")//produc/stock_actual
    return
        update value $actual with $actual + 10
    
```

   3. Muestra el nombre de la zona en lugar del código de la zona

```
let $zonas := doc("db/gimnasio/zonas.xml")//zona

for $p in doc("db/gimnasio/productos.xml")//produc
    return
        update value $p/cod_zona with data($zonas[cod_zona = $p/cod_zona]/nombre)
```

   4. Añade un título: <TITULO>DATOS DE LOS PRODUCTOS</TITULO>

```
update 
    insert <TITULO>DATOS DE LOS PRODUCTOS</TITULO> 
    following doc("db/gimnasio/productos.xml")//TITULO
```