1. Utilizando el documento sucursais.xml, realiza las siguientes consulta Xquery:
   1. Devuelve por cada sucursal, el código de sucursal y los datos de las cuentas con más saldo debe.
   
```
let $doc := doc("db/gimnasio/sucursales.xml")

for $suc in $doc//sucursal
    return
        <sucursal>
            <codigo>{data($suc/@codigo)}</codigo>
                {
                    for $cuenta in $suc/cuenta
                        return
                            if ($cuenta/saldodebe = max($suc//saldodebe)) then
                                $cuenta
                            else
                                ()
                }
        </sucursal>
```
   
   2. Devuelve la cuenta del tipo PENSIONES que hizo más aportaciones.

```
let $doc := doc("db/gimnasio/sucursales.xml")

for $suc in $doc//sucursal
    return
        $suc/cuenta[aportacion = max($suc//aportacion)][@tipo = "PENSIONES"]
```

2. El documento zonas.xml contiene información de las zonas donde se venden los productos del documento productos.xml. Utilizando esos dos documentos realiza las siguientes consultas Xquery:
   1. Obtén los datos denominación, precio y nombre de zona de cada producto, ordenado por nombre de zona.
   
```
let $productos := doc("db/gimnasio/productos.xml")//produc
let $zonas := doc("db/gimnasio/zonas.xml")//zona

for $p in $productos
    order by $zonas/nombre[../cod_zona = $p/cod_zona]
    return
        <datos>
            {$p/denominacion}
            {$p/precio}
            <zona>{data($zonas/nombre[../cod_zona = $p/cod_zona])}</zona>
        </datos>
```
   
   2. Obtén por cada zona, el nombre de zona y el número de productos que tienen.
   
```
let $productos := doc("db/gimnasio/productos.xml")//produc
let $zonas := doc("db/gimnasio/zonas.xml")//zona

for $zona in $zonas
    return
        <datos>
            {$zona/nombre}
            <productos>{count($productos[cod_zona = $zona/cod_zona])}</productos>
        </datos>
```
   
   3. Obtén por cada zona, el nombre de la zona, su código y el nombre del producto con menos stock actual.

```
let $productos := doc("db/gimnasio/productos.xml")//produc
let $zonas := doc("db/gimnasio/zonas.xml")//zona

for $zona in $zonas
    return
        <datos>
            {$zona/nombre}
            {$zona/cod_zona}
            {
                let $estos := $productos[cod_zona = $zona/cod_zona]
                return
                    $estos[stock_actual = min($estos/stock_actual)]
            }
        </datos>
```

3. Utiliza el documento sucursales.xml para realizar las siguientes consultas XQuery:
   1. Obtén por cada sucursal el mayor saldo haber y el nombre de la cuenta que tiene ese saldo.
   
```
let $suc := doc("db/gimnasio/sucursales.xml")//sucursal

for $s in $suc
    return
        <datos>
            <mayorsaldohaber>{max($s//saldohaber)}</mayorsaldohaber>
            {$s/cuenta/nombre[../saldohaber = max($s//saldohaber)]}
        </datos>
```

   2. Obtén por cada sucursal el nombre de la cuenta de tipo AHORRO tal que su saldo debe sea el máximo. Saca también el máximo.

```
let $suc := doc("db/gimnasio/sucursales.xml")//sucursal

for $s in $suc
    let $max := max($s//saldodebe[../@tipo = "AHORRO"])
    return
        <datos>
            {$s/cuenta/nombre[../@tipo = "AHORRO"][../saldodebe = $max]}
            <maximo>{$max}</maximo>
        </datos>
```

4. Utiliza los documentos produtos.xml y zonas.xml pra realizar las siguientes consultas XQuery:
   1. Visualiza los nombres de productos con su nombre de zona.
   
```
let $productos := doc("db/gimnasio/productos.xml")//produc
let $zonas := doc("db/gimnasio/zonas.xml")//zona

for $p in $productos
    return
        <datos>
            {$p/denominacion}
            <zona>{data($zonas/nombre[../cod_zona = $p/cod_zona])}</zona>
        </datos>
```
   
   2. Visualiza los nombres de productos con stock_minimo > 5, su código de zona, su nombre y el director de esa zona. Utiliza dos for en la consulta.

```
let $productos := doc("db/gimnasio/productos.xml")//produc
let $zonas := doc("db/gimnasio/zonas.xml")//zona

for $p in $productos
    return
        if ($p/stock_minimo > 5) then
            <datos>
                {$p/denominacion}
                {$p/cod_zona}
                {$zonas/nombre[../cod_zona = $p/cod_zona]}
                {$zonas/director[../cod_zona = $p/cod_zona]}
            </datos>
        else
            ()
```