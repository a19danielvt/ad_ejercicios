<!DOCTYPE html >
<%@page import="operations.*, java.util.*"%>
<html lang="es">
	<head>
		<title>Bootstrap Example</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    	<link rel="stylesheet" href="../cssFiles/myStyles.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
	</head>
	<body class="bg-primary">
		<nav class="navbar navbar-inverse">
			<div class="container.fluid">
				<div class="navbar-header">
					<a class="navbar-brand" href="../web1.html" style="font-size: 200%">My First Web</a>
				</div>
				<ul class="nav navbar-nav">
					<li class="dropdown">
						<a class="dropdown-toggle" data-toggle="dropdown" href="#">Options
							<span class="caret"></span>
						</a>
						<ul class="dropdown-menu">
							<li><a href="../studentsInputForm.html">Add student</a></li>
							<li><a href="studentsList.jsp">List students</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</nav>

        <div class="alert alert-info alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  			Si no se han aplicado los cambios, por favor, <strong>recargue la p&aacute;gina</strong>.
		</div>

		<div class="container">
            <h2>Students list</h2>
            <table class="table">
                <thead>
                    <tr>
                        <th>DNI</th>
                        <th>Name</th>
                        <th>Surname</th>
                        <th>Age</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <%
OperationsDB op = new OperationsDB();

try{
    op.openConnection();

    java.util.ArrayList students = (java.util.ArrayList) op.studentsList();
    
    int i = 0;
    for(Object o : students){
        Student s = (Student) o;
        out.println("<tr>");

        out.println("<td id='dni" + i + "'>" + s.getDni() + "</td>");
        out.println("<td>" + s.getName() + "</td>");
        out.println("<td>" + s.getSurname() + "</td>");
        out.println("<td>" + s.getAge() + "</td>");
        out.println("<td><img id='" + s.getDni() + "' onclick='return eliminar(this);' " + 
         "src='../images/icons8-eliminar-48.jpg' alt='eliminar' height='25px' width='25px'/>&nbsp; " +
         "<img id='" + s.getDni() + "' onclick='return modificar(this);' src='../images/icons8-editar-48.jpg' " + 
         "alt='editar' height='25px' width='25px'></td>");

        out.println("</tr>");
        i++;
    }

    op.closeConnection();

}catch(Exception e){
    out.println(e.getMessage());
}
                    %>
                </tbody>
            </table>
		</div>
        <script type="text/javascript">
            function eliminar(el){
                var conf = window.confirm("Are you sure you want to eliminate the student?");

                if(conf){
                    window.open("deleteStudent.jsp?dni=" + $(el).attr("id"), "_self")
                }
            }
            function modificar(m){
                window.open("modifyForm.jsp?dni=" + $(m).attr("id"), "_self")
            }
        </script>
 	</body>
</html> 
