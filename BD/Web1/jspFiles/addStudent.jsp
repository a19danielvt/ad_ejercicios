<!DOCTYPE html>
<%@page import="operations.*"%>
<html lang="es">
	<head>
		<title>Bootstrap Example</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    	<link rel="stylesheet" href="../cssFiles/myStyles.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
	</head>
	<body class="bg-primary">
		<nav class="navbar navbar-inverse">
			<div class="container.fluid">
				<div class="navbar-header">
					<a class="navbar-brand" href="../web1.html" style="font-size: 200%">My First Web</a>
				</div>
				<ul class="nav navbar-nav">
					<li class="dropdown">
						<a class="dropdown-toggle" data-toggle="dropdown" href="#">Options
							<span class="caret"></span>
						</a>
						<ul class="dropdown-menu">
							<li><a href="../studentsInputForm.html">Add student</a></li>
							<li><a href="studentsList.jsp">List students</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</nav>
		<p>
			<%

OperationsDB op = new OperationsDB();
try{
	op.openConnection();

	String dni = request.getParameter("dni");
	String nombre = request.getParameter("name");	
	String apellido = request.getParameter("surname");
	int edad = Integer.parseInt(request.getParameter("age"));

	Student s = new Student(dni, nombre, apellido, edad);

	op.addStudent(s);

			%>
		</p>
		<div class="alert alert-success">
  			<strong>Enhorabuena!</strong> El alumno se ha incluido en la base de datos.
		</div>
		<p>
			<%
	//out.println("Student added to the database");

	op.closeConnection();
}catch(Exception e){
	out.println("ERROR: " + e.getMessage());
}

			%>
		</p>
 	</body>
</html> 
