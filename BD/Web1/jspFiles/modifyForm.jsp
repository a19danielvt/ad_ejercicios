<!DOCTYPE html>
<%@page import="operations.*" %>
<html lang="es">

<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="../cssFiles/myStyles.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>

<body class="bg-primary">
    <nav class="navbar navbar-inverse">
        <div class="container.fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="../web1.html" style="font-size: 200%;">My First Web</a>
            </div>
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Options
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="../studentsInputForm.html">Add student</a></li>
                        <li><a href="studentsList.jsp">List students</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
    <%
    
    OperationsDB op = new OperationsDB();

    Student s = null;
    try{
        op.openConnection();

        s = op.getStudent(request.getParameter("dni"));        
    }catch(Exception e){
        out.println("Error al obtener el alumno: " + e.getMessage());
    }
    
    %>
    <form class="form-horizontal container-fluid" action="modifyStudent.jsp">
        <div class="form-group">
            <label class="control-label col-sm-4" for="dni">DNI:</label>
            <div class="col-sm-5">
            <%
            out.println("<input type='text' class='form-control' id='dni' name='dni' value='" + 
            s.getDni() + "' readonly>");
            %>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="name">Name:</label>
            <div class="col-sm-5">
            <%
            out.println("<input type='text' class='form-control' id='name' name='name' value='" + 
            s.getName() + "'>");
            %>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="surname">Surname:</label>
            <div class="col-sm-5">
            <%
            out.println("<input type='text' class='form-control' id='surname' name='surname' value='" + 
            s.getSurname() + "'>");
            %>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="age">Age:</label>
            <div class="col-sm-5">
            <%
            out.println("<input type='number' class='form-control' id='age' name='age' value='" + 
            s.getAge() + "'>");
            %>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-4 col-sm-8">
                <button type="submit" class="btn btn-default">Modify</button>
            </div>
        </div>
    </form>
</body>

</html>