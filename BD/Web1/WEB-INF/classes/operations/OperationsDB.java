package operations;

import java.sql.*;
import java.util.ArrayList;

public class OperationsDB {
    private Connection conn;

    public void openConnection() throws Exception{
        Class.forName("com.mysql.jdbc.Driver");
        String url = "jdbc:mysql://localhost/web1";
        conn = DriverManager.getConnection(url, "usuario", "abc123.");
    }

    public void closeConnection() throws Exception{
        conn.close();
    }

    public int addStudent(Student student) throws Exception{
        String dni = student.getDni();
        String nombre = student.getName();
        String apellido = student.getSurname();
        int edad = student.getAge();

        String cadenaDatos = "insert into student(dni, name, surname, age) values (?, ?, ?, ?)";

       
        PreparedStatement añadir = conn.prepareStatement(cadenaDatos);
        añadir.setString(1, dni);
        añadir.setString(2, nombre);
        añadir.setString(3, apellido);
        añadir.setInt(4, edad);

        return añadir.executeUpdate();
    }

    public Student getStudent(String dni) throws Exception{
        String cadena = "select * from student where DNI=?";

        String nombre = "";
        String apellido = "";
        int edad = 0;

        PreparedStatement consulta = conn.prepareStatement(cadena);

        consulta.setString(1, dni);

        ResultSet datos = consulta.executeQuery();
        if(datos.next()){
            nombre = datos.getString("name");
            apellido = datos.getString("surname");
            edad = datos.getInt("age");
        }

        return new Student(dni, nombre, apellido, edad);
    }

    public int deleteStudent(String dni) throws Exception{
        String cadena = "delete from student where DNI=?";

        PreparedStatement consulta = conn.prepareStatement(cadena);

        consulta.setString(1, dni);

        return consulta.executeUpdate();
    }

    public int modifyStudent(Student student) throws Exception{
        String cadena = "update student set name=?, "
            + "surname=?, age=? where dni=?";

        PreparedStatement consulta = conn.prepareStatement(cadena);
        consulta.setString(1, student.getName());
        consulta.setString(2, student.getSurname());
        consulta.setInt(3, student.getAge());
        consulta.setString(4, student.getDni());

        return consulta.executeUpdate();
    }

    public ArrayList<Student> studentsList() throws Exception{
        ArrayList<Student> students = new ArrayList<Student>();

        String cadena = "select * from student";

        PreparedStatement consulta = conn.prepareStatement(cadena);
        ResultSet datos = consulta.executeQuery();

        while(datos.next()){
            String dni = datos.getString("dni");
            String nombre = datos.getString("name");
            String apellido = datos.getString("surname");
            int edad = datos.getInt("age");

            students.add(new Student(dni, nombre, apellido, edad));
        }

        return students;
    }
}
