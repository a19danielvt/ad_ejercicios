import java.util.ArrayList;
import java.util.Scanner;

import dataBase.*;

/**
 * Program
 */
public class Program {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        Program p = new Program();
        Operations op = new Operations();
        ArrayList<Order> orders = new ArrayList<>();
        try{
            op.openConnection();
        }catch(Exception e){
            System.out.println("Fallo al abrir conexión con la base de datos: " + e.getMessage());
        }

        /**
         * Array de orders
         */
         try{
            orders = op.ordersList();

            //System.out.println(String.format("%-10s %-11s %-10s %-8s", "idOrder", "idProduct", "idClient", "amount"));

            //for(Order o : orders)
            //    System.out.println(o);
        }catch(Exception e){
            System.out.println("Fallo al crear ArrayList<Order>: " + e.getMessage());
        }
         

        /**
         * Buscar producto
         
        try {
            System.out.print("Introduce el id del producto a buscar: ");
            int id = sc.nextInt();

            if(op.existsProduct(id))
                System.out.println("Este producto sí que existe");
            else
                System.out.println("Este producto no existe");

        } catch (Exception e) {
            System.out.println("Fallo al buscar producto: " + e.getMessage());
        }
*/

        /**
         * Buscar cliente
         
        try {
            System.out.print("Introduce el id del cliente a buscar: ");
            int id = sc.nextInt();

            if(op.existsClient(id))
                System.out.println("Este cliente sí que existe");
            else
                System.out.println("Este cliente no existe");

        } catch (Exception e) {
            System.out.println("Fallo al buscar cliente: " + e.getMessage());
        }
*/

        /**
         * Añadir pedido
        try {
            System.out.println("CLIENTES ACTUALES----------\n");
            p.showClients(op);

            System.out.println();

            System.out.print("Id del cliente: "); int idClient = sc.nextInt();

            if(!op.existsClient(idClient))
                System.out.println("No existe ese cliente");
            else{
                System.out.println();
                System.out.println("PRODUCTOS ACTUALES----------\n");
                p.showProducts(op);

                System.out.print("\nId del producto: "); int idProduct = sc.nextInt();
                if(!op.existsProduct(idProduct))
                    System.out.println("No existe ese producto");
                else{
                    System.out.print("Cantidad: "); double amount = sc.nextDouble();
                    op.addOrder(idClient, idProduct, amount);
                    System.out.println("\nPEDIDO REALIZADO");
                }
            }
        } catch (Exception e) {
            System.out.println("Fallo al añadir pedido: " + e.getMessage());
        }
         */

        /**
         * Obtener pedido
        try {
            System.out.print("Id del pedido: "); int idOrder = sc.nextInt();

        } catch (Exception e) {
            System.out.println("Fallo al obtener pedido: " + e.getMessage());
        }

        try{
            op.closeConnection();
        }catch(Exception e){
            System.out.println("Fallo al cerrar conexión con la base de datos: " + e.getMessage());
        }
         */

        /**
         * Obtener datos pedido
        try{
            System.out.print("Obtener datos del pedido con id: "); int idOrder = sc.nextInt();
            Order o = op.getOrder(idOrder);
            if(o == null)
                System.out.println("No existe ese pedido");
            else{
                System.out.println("\n" + String.format("%-10s %-11s %-10s %-8s", "idOrder", "idProduct", "idClient", "amount"));
                System.out.println(o);
            }
        }catch(Exception e){
            System.out.println("Fallo al obtener pedido: " + e.getMessage());
        }
         */

        /**
         * Obtener datos producto
        try {
            System.out.print("Obtener datos del producto con id: "); int idProduct = sc.nextInt();
            Product product = op.getProduct(idProduct);

            if(product == null)
                System.out.println("No existe ese producto");
            else{
                System.out.println("\n" + String.format("%-11s %-15s %-20s %-7s %-30s", 
                    "idProduct", "name", "description", "price", "picture"));
                System.out.println(product);
            }
        } catch (Exception e) {
            System.out.println("Fallo al obtener producto: " + e.getMessage());
        }
         */

        /**
         * Eliminar pedido
        try {
            System.out.print("Introduce el id del pedido a eliminar: "); int idOrder = sc.nextInt();

            boolean borrar = false;
            for(Order o : orders){
                if(o.getIdOrder() == idOrder)
                    borrar = true;
            }

            if(borrar){
                op.deleteOrder(idOrder);
                System.out.println("PEDIDO ELIMINADO");
            }
            else
                System.out.println("Este pedido no existe.");
        } catch (Exception e) {
            System.out.println("Fallo al eliminar pedido: " + e.getMessage());
        }
         */

         /**
          * Modificar pedido
        try {
            System.out.print("Introduce el id del pedido a modificar: "); int idOrder = sc.nextInt();

            boolean modificar = false;

            for(Order o : orders){
                if(o.getIdOrder() == idOrder)
                    modificar = true;
            }

            if(modificar){   
                System.out.println("Nuevos datos:");
                System.out.print("\tidClient: "); int idClient = sc.nextInt();

                if(op.existsClient(idClient)){
                    Client c = op.getClient(idClient);

                    System.out.print("\tidProduct: "); int idProduct = sc.nextInt();
                    if(op.existsProduct(idProduct)){
                        Product product = op.getProduct(idProduct);

                        System.out.print("\tAmount: "); double amount = sc.nextDouble();
                        
                        op.modifyOrder(new Order(idOrder, c, product, amount));
                        System.out.println("PEDIDO MODIFICADO");
                    }
                    else
                        System.out.println("No existe el producto indicado.");
                }
                else
                    System.out.println("No existe el cliente indicado.");
            }
            else
                System.out.println("No existe el pedido indicado.");
        } catch (Exception e) {
            System.out.println("Fallo al modificar pedido: " + e.getMessage());
        }
          */
    }

    public void showProducts(Operations op) throws Exception{
        ArrayList<Product> products = op.productsList();

        System.out.println(String.format("%-11s %-15s %-20s %-7s %-30s", 
            "idProduct", "name", "description", "price", "picture"));

        for(Product pr : products)
            System.out.println(pr);
    }

    public void showClients(Operations op) throws Exception{
        ArrayList<Client> clients = op.clientsList();

        System.out.println(String.format("%-10s %-10s %-10s", "idClient", "dni", "name"));

        for(Client c : clients)
            System.out.println(c);
    }   
}