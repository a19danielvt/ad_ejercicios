package dataBase;

import java.sql.*;
import java.util.ArrayList;

/**
 * Operations
 */
public class Operations {
    private Connection conn;

    public void openConnection() throws Exception{
        Class.forName("com.mysql.jdbc.Driver");
        String url = "jdbc:mysql://192.168.56.3/orders";
        conn = DriverManager.getConnection(url, "usuario", "abc123.");
    }

    public void closeConnection() throws Exception{
        conn.close();
    }

    public ArrayList<Order> ordersList() throws Exception{
        ArrayList<Order> orders = new ArrayList<>();

        String seleccionarOrderr = "select * from orderr";
        PreparedStatement consulta = conn.prepareStatement(seleccionarOrderr);
        ResultSet datosOrderr = consulta.executeQuery();

        while(datosOrderr.next()){
            int idOrder = datosOrderr.getInt("idPedido");
            int idProduct = datosOrderr.getInt("idProduct");

            String seleccionarProduct = "select * from product where idProduct=?";
            consulta = conn.prepareStatement(seleccionarProduct);
            consulta.setInt(1, idProduct);
            ResultSet datosProduct = consulta.executeQuery();

            Product p = null;
            while(datosProduct.next()){
                String name = datosProduct.getString("name");
                String description = datosProduct.getString("description");
                double price = datosProduct.getDouble("price");
                String picture = datosProduct.getString("picture");

                p = new Product(idProduct, name, description, price, picture);
            }
            
            int idClient = datosOrderr.getInt("idClient");

            String seleccionarClient = "select * from client where idClient=?";
            consulta = conn.prepareStatement(seleccionarClient);
            consulta.setInt(1, idClient);
            ResultSet datosClient = consulta.executeQuery();

            Client c = null;
            while(datosClient.next()){
                String dni = datosClient.getString("dni");
                String name = datosClient.getString("name");

                c = new Client(idClient, dni, name);
            }

            double amount = datosOrderr.getDouble("amount");

            orders.add(new Order(idOrder, c, p, amount));
        }

        return orders;
    }

    public ArrayList<Product> productsList() throws Exception{
        ArrayList<Product> products = new ArrayList<>();

        String sentencia = "select * from product";
        PreparedStatement consulta = conn.prepareStatement(sentencia);
        ResultSet datos = consulta.executeQuery();

        while(datos.next()){
            int idProduct = datos.getInt("idProduct");
            String name = datos.getString("name");
            String description = datos.getString("description");
            double price = datos.getDouble("price");
            String picture = datos.getString("picture");

            products.add(new Product(idProduct, name, description, price, picture));
        }

        return products;
    }

    public ArrayList<Client> clientsList() throws Exception{
        ArrayList<Client> clients = new ArrayList<>();

        String sentencia = "select * from client";
        PreparedStatement consulta = conn.prepareStatement(sentencia);
        ResultSet datos = consulta.executeQuery();

        while(datos.next()){
            int idClient = datos.getInt("idClient");
            String dni = datos.getString("dni");
            String name = datos.getString("name");

            clients.add(new Client(idClient, dni, name));
        }

        return clients;
    }

    public boolean existsProduct(int id) throws Exception{
        String sentencia = "select * from product where idProduct=?";

        PreparedStatement consulta = conn.prepareStatement(sentencia);
        consulta.setInt(1, id);
        
        ResultSet datos = consulta.executeQuery();

        if(datos.next())
            return true;

        return false;
    }

    public boolean existsClient(int id) throws Exception{
        String sentencia = "select * from client where idClient=?";

        PreparedStatement consulta = conn.prepareStatement(sentencia);
        consulta.setInt(1, id);

        ResultSet datos = consulta.executeQuery();

        if(datos.next())
            return true;

        return false;
    }

    public void addOrder(int idClient, int idProduct, double amount) throws Exception{
        String sentencia = "insert into orderr(idProduct, idClient, amount) values (?, ?, ?)";
        PreparedStatement consulta = conn.prepareStatement(sentencia);

        consulta.setInt(1, idProduct);
        consulta.setInt(2, idClient);
        consulta.setDouble(3, amount);

        consulta.executeUpdate();
    }

    public Order getOrder(int idOrder) throws Exception{
        String sentencia = "select * from orderr where idPedido=?";
        PreparedStatement consulta = conn.prepareStatement(sentencia);
        consulta.setInt(1, idOrder);

        ResultSet datosOrderr = consulta.executeQuery();

        while(datosOrderr.next()){
            int idProduct = datosOrderr.getInt("idProduct");

            String seleccionarProduct = "select * from product where idProduct=?";
            consulta = conn.prepareStatement(seleccionarProduct);
            consulta.setInt(1, idProduct);
            ResultSet datosProduct = consulta.executeQuery();

            Product p = null;
            while(datosProduct.next()){
                String name = datosProduct.getString("name");
                String description = datosProduct.getString("description");
                double price = datosProduct.getDouble("price");
                String picture = datosProduct.getString("picture");

                p = new Product(idProduct, name, description, price, picture);
            }
            
            int idClient = datosOrderr.getInt("idClient");

            String seleccionarClient = "select * from client where idClient=?";
            consulta = conn.prepareStatement(seleccionarClient);
            consulta.setInt(1, idClient);
            ResultSet datosClient = consulta.executeQuery();

            Client c = null;
            while(datosClient.next()){
                String dni = datosClient.getString("dni");
                String name = datosClient.getString("name");

                c = new Client(idClient, dni, name);
            }

            double amount = datosOrderr.getDouble("amount");

            return new Order(idOrder, c, p, amount);
        }

        return null;
    }

    public Product getProduct(int idProduct) throws Exception{
        String sentencia = "select * from product where idProduct=?";

        PreparedStatement consulta = conn.prepareStatement(sentencia);
        consulta.setInt(1, idProduct);
        
        ResultSet datos = consulta.executeQuery();
        
        while(datos.next()){
            String name = datos.getString("name");
            String description = datos.getString("description");
            double price = datos.getDouble("price");
            String picture = datos.getString("picture");

            return new Product(idProduct, name, description, price, picture);
        }

        return null;
    }

    public Client getClient(int idClient) throws Exception{
        String sentencia = "select * from client where idClient=?";

        PreparedStatement consulta = conn.prepareStatement(sentencia);
        consulta.setInt(1, idClient);
        
        ResultSet datos = consulta.executeQuery();
        
        while(datos.next()){
            String dni = datos.getString("dni");
            String name = datos.getString("name");

            return new Client(idClient, dni, name);
        }
        
        return null;
    }

    public void deleteOrder(int idOrder) throws Exception{
        String sentencia = "delete from orderr where idPedido=?";
        PreparedStatement consulta = conn.prepareStatement(sentencia);
        consulta.setInt(1, idOrder);

        consulta.executeUpdate();
    }

    public void modifyOrder(Order o) throws Exception{
        String sentencia = "update orderr set idProduct=?, idClient=?, amount=? where idPedido=?";
        PreparedStatement consulta = conn.prepareStatement(sentencia);

        consulta.setInt(1, o.getProduct().getIdProduct());
        consulta.setInt(2, o.getClient().getIdClient());
        consulta.setDouble(3, o.getAmount());
        consulta.setInt(4, o.getIdOrder());

        consulta.executeUpdate();
    }
}