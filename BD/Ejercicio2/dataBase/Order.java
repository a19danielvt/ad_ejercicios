package dataBase;

/**
 * Order
 */
public class Order {
    private int idOrder;
    private Client client;
    private Product product;
    private double amount;

    public Order(int idOrder, Client client, Product product, double amount) {
        this.idOrder = idOrder;
        this.client = client;
        this.product = product;
        this.amount = amount;
    }

    public int getIdOrder() {
        return idOrder;
    }

    public void setIdOrder(int idOrder) {
        this.idOrder = idOrder;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
    
    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String toString(){
        return String.format("%-10s %-11s %-10s %-8s", idOrder, product.getIdProduct(), client.getIdClient(), amount);
    }
}