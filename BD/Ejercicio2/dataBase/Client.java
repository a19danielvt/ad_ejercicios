package dataBase;

/**
 * Client
 */
public class Client {
    private int idClient;
    private String dni;
    private String name;

    public Client(int idClient, String dni, String name) {
        this.idClient = idClient;
        this.dni = dni;
        this.name = name;
    }

    public int getIdClient() {
        return idClient;
    }

    public void setIdClient(int idClient) {
        this.idClient = idClient;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String toString(){
        return String.format("%-10s %-10s %-10s", idClient, dni, name);
    }
}