package dataBase;

/**
 * Product
 */
public class Product {
    int idProduct;
    String name;
    String description;
    double price;
    String picture;

    public Product(int idProduct, String name, String description, double price, String picture) {
        this.idProduct = idProduct;
        this.name = name;
        this.description = description;
        this.price = price;
        this.picture = picture;
    }

    public int getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(int idProduct) {
        this.idProduct = idProduct;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }    

    public String toString(){
        return String.format("%-11s %-15s %-20s %-7s %-30s", idProduct, name, description, price, picture);
    }
}