package Ejercicio8;

import org.w3c.dom.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import java.io.*;
import java.util.*;

import org.json.simple.*;
import org.json.simple.parser.*;

import com.thoughtworks.xstream.XStream;

public class Ejercicio8 {
    public static void main(String[] args) {
        ArrayList<Order> orders = leerJSON();
        createXmlXStream(orders);
        storeOrders(orders);
        createXml(orders);
        readOrders();
    }

    public static ArrayList<Order> leerJSON() {

        System.out.println("---------- DENTRO DEL JSON ----------");

        ArrayList<Order> arrayOrder = new ArrayList<>();

        try (FileReader fi = new FileReader("Ejercicio4/orders.json")) {
            JSONParser parser = new JSONParser();
            JSONObject doc = (JSONObject) parser.parse(fi);

            // orders
            JSONObject orders = (JSONObject) doc.get("orders");

            System.out.println("orders");

            // dentro de orders
            JSONArray ordersArray = (JSONArray) orders.get("order");

            for(Object o1 : ordersArray){
                // order
                JSONObject order = (JSONObject) o1;
                String tab = "\t";
                System.out.println(tab + "order");

                // dentro de order
                tab += "\t";
                // id
                long id = (long) order.get("id");
                System.out.println(tab + "id: " + id);     
                
                // price
                long price = (long) order.get("price");
                System.out.println(tab + "price: " + price);

                // delivered
                boolean delivered = (boolean) order.get("delivered");
                System.out.println(tab + "delivered: " + delivered);
                
                // client
                JSONObject client = (JSONObject) order.get("client");
                String dni = (String) client.get("DNI");
                String name = (String) client.get("name");
                String surname = (String) client.get("surname");

                Client c = new Client(dni, name, surname);

                System.out.println(tab + "client");
                System.out.println(tab + "\tDNI: " + dni);
                System.out.println(tab + "\tname: " + name);
                System.out.println(tab + "\tsurname: " + surname);

                // orderRows
                JSONObject orderRows = (JSONObject) order.get("orderRows");
                System.out.println(tab + "orderRows");

                // dentro de orderRows
                JSONArray arrayOrderRows = (JSONArray) orderRows.get("orderRow");

                ArrayList<OrderRow> ors = new ArrayList<>();

                for(Object o2 : arrayOrderRows){
                    String tab1 = tab + "\t";
                    // orderRow
                    JSONObject orderRow = (JSONObject) o2;
                    System.out.println(tab1 + "orderRow");

                    // dentro de orderRow
                    tab1 += "\t";
                    // amount
                    long amount = (long) orderRow.get("amount");
                    System.out.println(tab1 + "amount: " + amount);

                    // price
                    double priceRow = (double) orderRow.get("price");
                    System.out.println(tab1 + "price: " + priceRow);

                    // products
                    JSONObject products = (JSONObject) orderRow.get("products");
                    System.out.println(tab1 + "products");

                    // dentro de products
                    JSONArray arrayProducts = (JSONArray) products.get("product");

                    ArrayList<Product> ps = new ArrayList<Product>();

                    for(Object o3 : arrayProducts){
                        String tab2 = tab1 + "\t";
                        // product
                        JSONObject product = (JSONObject) o3;
                        long idProduct = (long) product.get("id");
                        String productName = (String) product.get("productName");
                        String description = (String) product.get("description");
                        String picture = (String) product.get("picture");
                        double priceProduct = (double) product.get("price");

                        Product p = new Product(idProduct, productName, description, picture, priceProduct);
                        ps.add(p);

                        System.out.println(tab2 + "product");

                        System.out.println(tab2 + "\t" + "id: " + idProduct);
                        System.out.println(tab2 + "\t" + "productName: " + productName);
                        System.out.println(tab2 + "\t" + "description: " + description);
                        System.out.println(tab2 + "\t" + "picture: " + picture);
                        System.out.println(tab2 + "\t" + "price: " + priceProduct);

                    }

                    OrderRow o = new OrderRow(amount, priceRow, ps);
                    ors.add(o);
                }

                Order or = new Order(id, price, delivered, c, ors);
                arrayOrder.add(or);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return arrayOrder;
    }

    public static void createXmlXStream(ArrayList<Order> array){
        try {
            XStream x = new XStream();
            x.setMode(XStream.NO_REFERENCES);
            x.alias("orders", List.class);
            x.alias("order", Order.class);
            x.alias("orderRow", OrderRow.class);
            x.alias("client", Client.class);
            x.alias("product", Product.class);
            x.toXML(array, new FileOutputStream("ordersXStream.xml"));
        } catch (Exception e) {
            System.out.println("Fallo al crear el XStream");
        }
    }

    public static void createXml(ArrayList<Order> array){
        try{
            DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document doc = db.getDOMImplementation().createDocument(null, "orders", null);
            doc.setXmlVersion("1.0");

            Element raiz = doc.getDocumentElement();

            for(Order o : array){
                //order
                Element order = doc.createElement("order");
                raiz.appendChild(order);

                //----------dentro de order
                //id
                Element idOrder = doc.createElement("id");
                Text textOrder = doc.createTextNode(o.getId() + "");
                idOrder.appendChild(textOrder);
                order.appendChild(idOrder);

                //price
                Element priceOrder = doc.createElement("price");
                Text textPrice = doc.createTextNode(o.getPrice() + "");
                priceOrder.appendChild(textPrice);
                order.appendChild(priceOrder);

                //delivered
                Element delivered = doc.createElement("delivered");
                Text textDelivered = doc.createTextNode(o.isDelivered() + "");
                delivered.appendChild(textDelivered);
                order.appendChild(delivered);

                //client
                Element client = doc.createElement("client");
                order.appendChild(client);

                //----------dentro de client
                //dni
                Element dni = doc.createElement("dni");
                Text textDni = doc.createTextNode(o.getCliente().getDni());
                dni.appendChild(textDni);
                client.appendChild(dni);

                //name
                Element name = doc.createElement("name");
                Text textName = doc.createTextNode(o.getCliente().getName());
                name.appendChild(textName);
                client.appendChild(name);

                //surname
                Element surname = doc.createElement("surname");
                Text textSurname = doc.createTextNode(o.getCliente().getSurname());
                surname.appendChild(textSurname);
                client.appendChild(surname);

                //----------acaba client
                //orderRows
                Element orderRows = doc.createElement("orderRows");
                order.appendChild(orderRows);

                //----------dentro de orderRows
                ArrayList<OrderRow> orders = o.getOrderRows();

                for(OrderRow or : orders){
                    //orderRow
                    Element orderRow = doc.createElement("orderRow");
                    orderRows.appendChild(orderRow);

                    //----------dentro de orderRow
                    //amount
                    Element amount = doc.createElement("amount");
                    Text textAmount = doc.createTextNode(or.getAmount() + "");
                    amount.appendChild(textAmount);
                    orderRow.appendChild(amount);

                    //price
                    Element priceRow = doc.createElement("price");
                    Text textPriceRow = doc.createTextNode(or.getPrice() + "");
                    priceRow.appendChild(textPriceRow);
                    orderRow.appendChild(priceRow);

                    //products
                    Element products = doc.createElement("products");
                    orderRow.appendChild(products);

                    //----------dentro de products
                    ArrayList<Product> ps = or.getProductos();

                    for(Product p : ps){
                        //product
                        Element product = doc.createElement("product");
                        products.appendChild(product);

                        //----------dentro de product
                        //id
                        Element idProduct = doc.createElement("id");
                        Text textIdProduct = doc.createTextNode(p.getId() + "");
                        idProduct.appendChild(textIdProduct);
                        product.appendChild(idProduct);

                        //productName
                        Element productName = doc.createElement("productName");
                        Text textProductName = doc.createTextNode(p.getProductName() + "");
                        productName.appendChild(textProductName);
                        product.appendChild(productName);

                        //description
                        Element description = doc.createElement("description");
                        Text textDescription = doc.createTextNode(p.getDescription() + "");
                        description.appendChild(textDescription);
                        product.appendChild(description);

                        //picture
                        Element picture = doc.createElement("picture");
                        Text textPicture = doc.createTextNode(p.getPicture() + "");
                        picture.appendChild(textPicture);
                        product.appendChild(picture);

                        //price
                        Element priceProduct = doc.createElement("priceProduct");
                        Text textPriceProduct = doc.createTextNode(p.getPrice() + "");
                        priceProduct.appendChild(textPriceProduct);
                        product.appendChild(priceProduct);
                    }
                }
            }

            Source fuente = new DOMSource(doc);
            Result resultado = new StreamResult(new File("orders.xml"));
            Transformer t = TransformerFactory.newInstance().newTransformer();
            t.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            t.transform(fuente, resultado);
        }catch(Exception e){System.out.println("Fallo al crear el documento xml");}
    }

    public static void storeOrders(ArrayList<Order> array){
        try{
            ObjectOutputStream oo = new ObjectOutputStream(new FileOutputStream("orders.dat"));

            for(Order o : array){
                oo.writeObject(o);
            }

            oo.close();
        }catch(Exception e){System.out.println("Fallo al crear el archivo de texto: " + e.getMessage());}
    }

    public static void readOrders(){
        try(FileInputStream fi = new FileInputStream("orders.dat");
        ObjectInputStream oi = new ObjectInputStream(fi)){

        System.out.println("---------- DENTRO DEL DAT ----------");

        System.out.println("orders");

            while(fi.available() > 0){
                Order o = (Order) oi.readObject();
                System.out.println("\torder");

                System.out.println("\t\tid: " + o.getId());
                System.out.println("\t\tprice: " + o.getPrice());
                System.out.println("\t\tdelivered: " + o.isDelivered());
                System.out.println("\t\tclient");

                Client c = o.getCliente();

                System.out.println("\t\t\tDNI: " + c.getDni());
                System.out.println("\t\t\tname: " + c.getName());
                System.out.println("\t\t\tsurname: " + c.getSurname());

                System.out.println("\t\torderRows");

                ArrayList<OrderRow> orderRows = o.getOrderRows();

                for(OrderRow or : orderRows){
                    System.out.println("\t\t\torderRow");

                    System.out.println("\t\t\t\tamount: " + or.getAmount());
                    System.out.println("\t\t\t\tprice: " + or.getPrice());
                    System.out.println("\t\t\t\tproducts");

                    ArrayList<Product> ps = or.getProductos();

                    for(Product p : ps){
                        System.out.println("\t\t\t\t\tproduct");

                        System.out.println("\t\t\t\t\t\tid: " + p.getId());
                        System.out.println("\t\t\t\t\t\tproductName: " + p.getProductName());
                        System.out.println("\t\t\t\t\t\tdescription: " + p.getDescription());
                        System.out.println("\t\t\t\t\t\tpicture: " + p.getPicture());
                        System.out.println("\t\t\t\t\t\tprice: " + p.getPrice());
                    }
                }
            }

        }catch(Exception e){System.out.println("Fallo en readOrders: " + e.getMessage());}
    }
}
