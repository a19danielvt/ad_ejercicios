package Ejercicio8;

import java.io.Serializable;
import java.util.ArrayList;

public class OrderRow implements Serializable{
    private static final long serialVersionUID = 1L;
    long amount;
    double price;
    ArrayList<Product> productos;

    public OrderRow(long amount, double price, ArrayList<Product> productos) {
        this.amount = amount;
        this.price = price;
        this.productos = productos;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public ArrayList<Product> getProductos() {
        return productos;
    }

    public void setProductos(ArrayList<Product> productos) {
        this.productos = productos;
    }

    
}
