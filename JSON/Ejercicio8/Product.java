package Ejercicio8;

import java.io.Serializable;

public class Product implements Serializable {
    private static final long serialVersionUID = 1L;
    long id;
    String productName;
    String description;
    String picture;
    double price;

    public Product(long id, String productName, String description, String picture, double price) {
        this.id = id;
        this.productName = productName;
        this.description = description;
        this.picture = picture;
        this.price = price;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
