package Ejercicio8;

import java.io.Serializable;
import java.util.ArrayList;

public class Order implements Serializable{
    private static final long serialVersionUID = 1L;
    private long id;
    private double price;
    private boolean delivered;
    private Client cliente;
    private ArrayList<OrderRow> orderRows;

    public Order(long id, double price, boolean delivered, Client cliente, ArrayList<OrderRow> orderRows) {
        this.id = id;
        this.price = price;
        this.delivered = delivered;
        this.cliente = cliente;
        this.orderRows = orderRows;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public boolean isDelivered() {
        return delivered;
    }

    public void setDelivered(boolean delivered) {
        this.delivered = delivered;
    }

    public Client getCliente() {
        return cliente;
    }

    public void setCliente(Client cliente) {
        this.cliente = cliente;
    }

    public ArrayList<OrderRow> getOrderRows() {
        return orderRows;
    }

    public void setOrderRows(ArrayList<OrderRow> orderRows) {
        this.orderRows = orderRows;
    }
}
