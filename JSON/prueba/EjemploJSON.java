package prueba;

import org.json.simple.*;
import org.json.simple.parser.*;
import java.io.*;

public class EjemploJSON {
    public static void main(String[] args) {
        createJSON(); 
        readJSON(); 
    }

    public static void readJSON(){
        try (FileReader fi = new FileReader("prueba.json")){
            // obtiene el documento
            JSONParser parser = new JSONParser();
            JSONObject doc = (JSONObject) parser.parse(fi);

            String nombre = (String) doc.get("name");
            double salario = (double) doc.get("salary");
            long edad = (long) doc.get("age");

            System.out.println(nombre + " " + salario + " " + edad);

            JSONArray array = (JSONArray) doc.get("array");

            for(Object j : array){
                System.out.print(j + ", ");
            }
            System.out.println();

            //System.out.println(array);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    
    private static void createJSON(){
        try{
            // Crea el objeto
            JSONObject o = new JSONObject();
            o.put("name", "Luisa");
            o.put("salary", new Double(600.5));
            o.put("age", new Integer(20));

            // Crear array
            JSONArray a = new JSONArray();
            a.add("texto");
            a.add(new Integer(1));
            a.add(new Double(23));

            // Añadir array
            o.put("array", a);

            // Crea el archivo
            FileWriter f = new FileWriter("prueba.json");
            f.write(o.toJSONString());
            
            f.close();
        }catch(Exception e){System.err.println("FALLO EN createJSON()");}
    }
}
