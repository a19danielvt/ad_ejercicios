package Ejercicio6;

import java.io.FileReader;
import org.json.simple.*;
import org.json.simple.parser.*;

public class Ejercicio6 {
    public static void main(String[] args) {
        leerJSON();
    }

    public static void leerJSON(){
        try (FileReader fi = new FileReader("Ejercicio1/Ejercicio1.json")){
            // doc
            JSONParser parser = new JSONParser();
            JSONObject doc = (JSONObject) parser.parse(fi);

            // menu
            JSONObject menu = (JSONObject) doc.get("menu");
            System.out.println("menu");

            // dentro de menu
            String id = (String) menu.get("id");
            String value = (String) menu.get("value");
            JSONObject popup = (JSONObject) menu.get("popup");

            String tab = "\t";

            System.out.println(tab + "id: " + id);
            System.out.println(tab + "value: " + value);
            System.out.println(tab + "popup");
            
            //dentro de popup
            JSONArray menuitem = (JSONArray) popup.get("menuitem");

            tab += "\t";

            //dentro de menuitem
            for(Object o : menuitem){
                JSONObject ob = (JSONObject) o;

                System.out.println(tab + "menuitem");

                String temp = tab + "\t";
                
                System.out.println(temp + "value: " + ob.get("value"));
                System.out.println(temp + "onClick: " + ob.get("onClick"));
            }
        } catch (Exception e) {
            //TODO: handle exception
        }
    }
}
