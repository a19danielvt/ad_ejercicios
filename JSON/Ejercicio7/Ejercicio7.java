package Ejercicio7;

import java.io.FileWriter;

import org.json.simple.*;
import org.json.simple.parser.*;

public class Ejercicio7 {
    public static void main(String[] args) {
        try{
            crearJSON();
        }catch(Exception e){System.out.println(e.getMessage());}
    }

    public static void crearJSON() throws Exception{
        JSONObject producto00 = new JSONObject();
        producto00.put("product", datosProducto(1, "Nombre 1", "Descripción", "imagen", 1.1));

        JSONObject producto10 = new JSONObject();
        producto10.put("product", datosProducto(2, "Nombre 2", "Descripción", "imagen", 1.2));      

        JSONArray productos0 = new JSONArray();
        productos0.add(producto00);

        JSONArray productos1 = new JSONArray();
        productos1.add(producto10);
        
        JSONObject orderRow0 = new JSONObject();
        orderRow0.put("orderRow", datosOrderRow(productos0, 1, 1.1));

        JSONObject orderRow1 = new JSONObject();
        orderRow1.put("orderRow", datosOrderRow(productos1, 2, 1.2));

        JSONArray orderRows = new JSONArray();
        orderRows.add(orderRow0);
        orderRows.add(orderRow1);

        JSONObject order0 = new JSONObject();
        order0.put("order", datosOrder(1, true, 11, datosClient("11111111-A", "Nombre", "Apellido"), orderRows));

        JSONArray orders = new JSONArray();
        orders.add(order0);
        
        JSONObject root = new JSONObject();
        root.put("orders", orders);

        FileWriter fw = new FileWriter("ejercicio7.json");
        fw.write(root.toJSONString());
        fw.close();
    }
    
    public static JSONObject datosProducto(int id, String name, String description, String picture, double price){
        JSONObject datos = new JSONObject();
        datos.put("id", id);
        datos.put("productName", name);
        datos.put("description", description);
        datos.put("picture", picture);
        datos.put("price", price);

        return datos;
    }

    public static JSONObject datosOrderRow(JSONArray products, int amount, double price){
        JSONObject datos = new JSONObject();
        datos.put("products", products);
        datos.put("amount", amount);
        datos.put("price", price);

        return datos;
    }

    public static JSONObject datosClient(String dni, String name, String surname){
        JSONObject datos = new JSONObject();
        datos.put("dni", dni);
        datos.put("name", name);
        datos.put("surname", surname);

        return datos;
    }

    public static JSONObject datosOrder(int id, boolean delivered, int price, JSONObject client, JSONArray orderRows){
        JSONObject datos = new JSONObject();
        datos.put("id", id);
        datos.put("delivered", delivered);
        datos.put("price", price);
        datos.put("client", client);
        datos.put("orderRows", orderRows);

        return datos;
    }
}