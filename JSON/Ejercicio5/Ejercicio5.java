package Ejercicio5;

import java.io.FileWriter;
import org.json.simple.*;
import org.json.simple.parser.*;

public class Ejercicio5 {
    public static void main(String[] args) {
        createJSON();
    }

    public static void createJSON(){
        try{
            // datos del array menuitem
            JSONObject menuitem0 = new JSONObject();
            menuitem0.put("value", "New");
            menuitem0.put("onClick", "CreateNewDoc()");

            JSONObject menuitem1 = new JSONObject();
            menuitem1.put("value", "Open");
            menuitem1.put("onClick", "OpenDoc()");

            JSONObject menuitem2 = new JSONObject();
            menuitem2.put("value", "Close");
            menuitem2.put("onClick", "CloseDoc()");

            // array menuitem
            JSONArray menuitem = new JSONArray();
            menuitem.add(menuitem0);
            menuitem.add(menuitem1);
            menuitem.add(menuitem2);

            // popup
            JSONObject popup = new JSONObject();
            popup.put("menuitem", menuitem);

            // datos menu
            JSONObject menuDatos = new JSONObject();
            menuDatos.put("id", "file");
            menuDatos.put("value", "File");
            menuDatos.put("popup", popup);

            // menu
            JSONObject menu = new JSONObject();
            menu.put("menu", menuDatos);

            // crea el archivo
            FileWriter fw = new FileWriter("ejercicio5.json");
            fw.write(menu.toString());
            fw.close();
        }catch(Exception e){
            System.out.println(e.getMessage());
        };
    }
}
