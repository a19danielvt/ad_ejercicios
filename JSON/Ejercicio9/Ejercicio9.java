package Ejercicio9;

import org.json.simple.*;
import org.json.simple.parser.*;
import org.w3c.dom.*;
import javax.xml.parsers.*;
import javax.xml.stream.events.Attribute;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import java.io.*;
import java.util.*;
import com.thoughtworks.xstream.XStream;

public class Ejercicio9 {
    public static void main(String[] args) {
        ArrayList<Client> clientes = leerClients();
        clientsDAT(clientes);
        XStream(clientes);
        DOMtoXML(clientes);
        deleteAddresses();
    }

    public static ArrayList<Client> leerClients() {
        ArrayList<Client> clientes = new ArrayList<>();

        try (FileReader fi = new FileReader("Ejercicio3/clients.json")) {
            // doc
            JSONParser parser = new JSONParser();
            JSONObject doc = (JSONObject) parser.parse(fi);

            // clients
            JSONObject clients = (JSONObject) doc.get("clients");
            System.out.println("clients");

            // arrayClients
            JSONArray clientsArray = (JSONArray) clients.get("client");

            for (Object o : clientsArray) {
                // client
                JSONObject client = (JSONObject) o;
                System.out.println("\tclient");

                // dentro de client
                // dni
                String dni = (String) client.get("dni");
                System.out.println("\t\tdni: " + dni);

                // name
                String name = (String) client.get("name");
                System.out.println("\t\tname: " + name);

                // surname
                String surname = (String) client.get("surname");
                System.out.println("\t\tsurname: " + surname);

                // addresses
                JSONObject addresses = (JSONObject) client.get("addresses");
                System.out.println("\t\taddresses");

                // arrayAddresses
                JSONArray addressesArray = (JSONArray) addresses.get("address");

                ArrayList<Address> direcciones = new ArrayList<>();

                for (Object o1 : addressesArray) {
                    // address
                    JSONObject address = (JSONObject) o1;
                    System.out.println("\t\t\taddress");

                    // dentro de address
                    // street
                    String street = (String) address.get("street");
                    System.out.println("\t\t\t\tstreet: " + street);

                    // number
                    String number = (String) address.get("number");
                    System.out.println("\t\t\t\tnumber: " + number);

                    // postalCode
                    String postalCode = (String) address.get("postalCode");
                    System.out.println("\t\t\t\tpostalCode: " + postalCode);

                    direcciones.add(new Address(street, number, postalCode));
                }

                clientes.add(new Client(name, surname, dni, direcciones));
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return clientes;
    }

    public static void clientsDAT(ArrayList<Client> clientes) {
        try {
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("clients.dat"));

            for (Client c : clientes) {
                oos.writeObject(c);
            }

            oos.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static void XStream(ArrayList<Client> clientes) {
        try {
            XStream x = new XStream();
            x.setMode(XStream.NO_REFERENCES);
            x.alias("clients", List.class);
            x.alias("client", Client.class);
            x.alias("address", Address.class);
            x.toXML(clientes, new FileOutputStream("clientsXStream.xml"));
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static void DOMtoXML(ArrayList<Client> clientes) {
        try {
            DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document doc = db.getDOMImplementation().createDocument(null, "clients", null);
            doc.setXmlVersion("1.0");

            for(Client c : clientes){
                //client
                Element client = doc.createElement("client");

                client.setAttribute("dni", c.getDni());
                client.setAttribute("name", c.getName());
                client.setAttribute("surname", c.getSurname());

                doc.getDocumentElement().appendChild(client);

                ArrayList<Address> addresses = c.getAddresses();

                for(Address a : addresses){
                    Element address = doc.createElement("address");

                    address.setAttribute("postalCode", a.getPostalCode());
                    client.appendChild(address);

                    Element street = doc.createElement("street");
                    Text streetText = doc.createTextNode(a.getStreet());
                    street.appendChild(streetText);
                    address.appendChild(street);

                    Element number = doc.createElement("number");
                    Text numberText = doc.createTextNode(a.getNumber());
                    number.appendChild(numberText);
                    address.appendChild(number);
                }
            }

            Source fuente = new DOMSource(doc);
            Result result = new StreamResult(new File("clients.xml"));

            Transformer t = TransformerFactory.newInstance().newTransformer();
            t.transform(fuente, result);
        } catch (Exception e) {
            //TODO: handle exception
        }
    }

    public static void deleteAddresses(){
        try{
            DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document doc = db.parse(new File("clients.xml"));
            doc.getDocumentElement().normalize();

            NodeList direcciones = doc.getElementsByTagName("address");

            while(direcciones.getLength() > 0){
                Element element = (Element) direcciones.item(0);
                Element parent = (Element) element.getParentNode();
                parent.removeChild(element);
            }

            Source fuente = new DOMSource(doc);
            Result result = new StreamResult(new File("clientsNoAddress.xml"));

            Transformer t = TransformerFactory.newInstance().newTransformer();
            t.transform(fuente, result);
        }catch(Exception e){System.out.println(e.getMessage());}
    }
}