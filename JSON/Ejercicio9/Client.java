package Ejercicio9;

import java.io.Serializable;
import java.util.ArrayList;

public class Client implements Serializable{
    private static final long serialVersionUID = 1L;
    private String name;
    private String surname;
    private String dni;
    private ArrayList<Address> addresses;

    public Client(String name, String surname, String dni, ArrayList<Address> addresses) {
        this.name = name;
        this.surname = surname;
        this.dni = dni;
        this.addresses = addresses;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public ArrayList<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(ArrayList<Address> addresses) {
        this.addresses = addresses;
    }
}
